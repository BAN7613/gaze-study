﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomInformation : MonoBehaviour
{
    [Record("Field Number")]
    public int number1;

    [Record("Property Number")]
    public int number2 { get; set; }

    [Record("Random Text")]
    public string textInfo = "Initial";

    [Record("Interval Number", 1f)]
    public int intervalNumber;

    // Use this for initialization
    void Start ()
	{
	    number1 = 1;
	    number2 = 2;
        intervalNumber = 3;
	}
	
	// Update is called once per frame
	void Update () {
		
        // Randomly change numbers
	    if (Random.Range(0, 100) < 2)
	    {
	        number1 = Random.Range(0, 10);
	    }

	    if (Random.Range(0, 100) < 2)
	    {
	        number2 = Random.Range(0, 10);
	    }

        if (Random.Range(0, 100) < 2)
        {
            textInfo = textInfo + Random.Range(0,10);
        }

        if (Random.Range(0, 100) < 2)
        {
            intervalNumber = Random.Range(0, 10);
        }
    }
}
