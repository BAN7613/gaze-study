﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using RockVR.Video;
using UnityEngine;
using UnityEngine.XR.WSA.Persistence;

/// <summary>
/// Manages file output and buffering for csv and json file.
/// </summary>
public class RecordingModel
{
    private string _csv_path;

    public string FilenameBaseString { get; private set; }

    // Do i need a backup for this?
    private string _json_path;

    // Lines not yet saved to file
    private List<string> FloatingLines;

    // Data of current line
    private SortedDictionary<int, object> CurrentFrame;

    // Keep track of number of variables
    private int MaxVars = 1;

    public RecordingModel(string filename)
    {
        FloatingLines = new List<string>();
        CurrentFrame = new SortedDictionary<int, object>();

        this.FilenameBaseString = filename = DateTime.Now.ToString("yyyy_MM_dd__hh_mm_ss") + "_" + filename;
        this._csv_path = @"results/" + filename + ".csv";
        this._json_path = @"results/" + filename + ".json";

        string folder = @"results";
        if (!Directory.Exists(folder))
        {
            Directory.CreateDirectory(folder);
        }
    }

    public void NewFrame(float time)
    {
        string line = "";
        int ci = 0;
        foreach (var o in CurrentFrame)
        {
            while (o.Key > ci++)
                line += ";";
            line += o.Value;
            line += ";";
        }
        FloatingLines.Add(line);
        CurrentFrame.Clear();
        CurrentFrame.Add(0, "" + ToUnixTime(System.DateTime.Now));
        CurrentFrame.Add(1, "" + time);
    }

    public void SaveToFile()
    {
        StreamWriter writer = new StreamWriter(_csv_path, true);
        foreach (string s in FloatingLines)
        {
            writer.WriteLine(s);
        }
        writer.Close();
      //  Debug.Log("Wrote " + FloatingLines.Count + " to file.");
        FloatingLines.Clear();
    }

    public void SaveJSON(RecordingInfo info)
    {
        StreamWriter writer = new StreamWriter(_json_path, false);
        string json = JsonConvert.SerializeObject(info, Formatting.Indented);
        Debug.Log(json);
        writer.WriteLine(json);
        writer.Close();
        Debug.Log("Wrote JSON to file.");
    }

    public void RecordVar(int id, object value)
    {
        //Debug.Log("Record: " + id);
        MaxVars = Mathf.Max(id, MaxVars);
        CurrentFrame.Add(id, value);
    }

    public void RecordGameObjectNow(int id, GameObject go)
    {
        MaxVars = Mathf.Max(id, MaxVars);
        CurrentFrame.Add(id, go.transform.position);
    }

    private static readonly DateTime Epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
    public static long ToUnixTime(DateTime date)
    {
        return Convert.ToInt64((date.ToUniversalTime() - Epoch).TotalMilliseconds);
    }

}
