﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using RockVR.Video;
using UnityEngine;
using UnityEngine.XR.WSA.Persistence;

/// <summary>
/// Class that handles planning the recording for all objects and types.
/// Used to start and finish the recording.
/// Manages VideoCapture via RockVR Plugin.
/// </summary>
[RequireComponent(typeof(MainRecorder))]
[RequireComponent(typeof(VideoCaptureCtrl))]
public class RecordingController : MonoBehaviour
{
    public string ExperimentName = "DemoExperiment";
    public int SubjectID = 0;

    private List<MonoBehaviourRecording> MBRecordings;
    private List<GameObjectRecording> GameObjectRecordings;

    private RecordingModel Model;
    public RecordingInfo Info;

    private RecordingStatus Status;
    private VideoCaptureCtrl _videoCapture;

    private float nextSaveTime = 1f;

    public static RecordingController Instance = null;
    public static bool QuitAfterSave = false;

    private Transform headset, lController, rController;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);

        _videoCapture = GetComponent<VideoCaptureCtrl>();
        MBRecordings = new List<MonoBehaviourRecording>();
        GameObjectRecordings = new List<GameObjectRecording>();
        Model = new RecordingModel(ExperimentName);
        Status = RecordingStatus.NotStarted;
        Info = new RecordingInfo(DateTime.Now);

        SetupVideoCapture();
    }

    [RuntimeInitializeOnLoadMethod]
    static void RunOnStart()
    {
        Application.wantsToQuit += WantsToQuit;
    }

    private void SetupVideoCapture()
    {
        //Camera cam = Camera.main;
        Camera cam = null;
        try
        {
            var steamCam = Camera.main.gameObject;
            cam = steamCam.gameObject.GetComponent<Camera>();
        }
        catch (NullReferenceException e)
        {
            //Debug.LogWarning("Camera not found!");
            StartCoroutine(FindCamera());
            return;
        }

        if (cam != null)
        {
            var listener = cam.GetComponent<AudioListener>();
            if (listener != null)
            {
                var audioCapture = listener.gameObject.AddComponent<AudioCapture>();
                _videoCapture.audioCapture = audioCapture;
            }
            else
            {
                listener = FindObjectOfType<AudioListener>();
                if (listener != null)
                {
                    var audioCapture = listener.gameObject.AddComponent<AudioCapture>();
                    _videoCapture.audioCapture = audioCapture;
                }
            }

            // Add a dedicated Camera for VideoCapture
            var vidCam = FindObjectOfType<CameraForVideo>();
            vidCam.transform.parent = cam.transform;

            //var videoCapture = cam.gameObject.AddComponent<VideoCapture>();
            var videoCapture = vidCam.GetComponent<VideoCapture>();

            videoCapture.customPath = true;
            videoCapture.customPathFolder = @"results/";
            PathConfig.SaveFolder = @"results/";
            PathConfig.finalVideoName = Model.FilenameBaseString + ".mp4";
            //videoCapture.isDedicated = false;

            Info.AddSetting("videoFileName", PathConfig.finalVideoName);

            VideoCaptureBase[] captures = new VideoCaptureBase[] { videoCapture };
            _videoCapture.videoCaptures = captures;
            Debug.Log("Camera found!");
        }
        else
        {
            Debug.LogWarning("Camera not found!");
            StartCoroutine(FindCamera());
        }
    }

    private IEnumerator FindCamera()
    {
        yield return new WaitForEndOfFrame();
        SetupVideoCapture();
    }

    public void StartRecording()
    {
        if (Status != RecordingStatus.NotStarted)
        {
            Debug.LogError("Recording was already started!");
            return;
        }

        Info.AddSetting("experiment_name", ExperimentName);
        Info.AddSetting("subjectid", "" + SubjectID);

        Status = RecordingStatus.Recording;
        CaptureScreenshotForOverview();
        //_videoCapture.StartCapture();
    }

    // Uses the screenshot camera to capture a screenshot
    private void CaptureScreenshotForOverview()
    {
        var recorder = FindObjectOfType<ScreenRecorder>();
        if (recorder != null)
        {
            recorder.OnScreenshotSaved += OnScreenshotSaved;
            recorder.CaptureScreenshot();
        }
    }

    // Save screenshot in info file
    private void OnScreenshotSaved(byte[] pngBytes)
    {
        var recorder = FindObjectOfType<ScreenRecorder>();
        if (recorder != null)
        {
            var cam = recorder.GetComponent<Camera>();
            Info.AddOverviewCamera(cam, pngBytes);
        }
    }

    // Adds VR devices to recording
    internal void setVRObjects(Transform headset, Transform lController, Transform rController)
    {
        if (this.headset == null && headset != null)
        {
            this.headset = headset;
            AddGameObjectRecording(headset.gameObject, RecVars.X | RecVars.Y | RecVars.Z | RecVars.RX | RecVars.RY | RecVars.RZ);
        }
        if (this.lController == null && lController != null)
        {
            this.lController = lController;
            AddGameObjectRecording(lController.gameObject, RecVars.X | RecVars.Y | RecVars.Z | RecVars.RX | RecVars.RY | RecVars.RZ);
        }

        if (this.rController == null && rController != null)
        {
            this.rController = rController;
            AddGameObjectRecording(rController.gameObject, RecVars.X | RecVars.Y | RecVars.Z | RecVars.RX | RecVars.RY | RecVars.RZ);
        }
    }

    /// <summary>
    /// Stops recording and finishes up saving all files.
    /// </summary>
    public void StopRecording()
    {
        if (Status == RecordingStatus.Recording)
        {
            Debug.Log("Stopping Recording");
            if (_videoCapture.status == VideoCaptureCtrlBase.StatusType.STARTED)
            {
                Status = RecordingStatus.FinishingUp;
                _videoCapture.eventDelegate.OnComplete += VideoCaptureCompleted;
                _videoCapture.eventDelegate.OnError += VideoCaptureError;
                _videoCapture.StopCapture();
            }
            else
            {
                Status = RecordingStatus.Ended;
            }
            Model.SaveToFile();
        }
    }
    
    private void VideoCaptureError(int error)
    {

        Status = RecordingStatus.Ended;
        Debug.LogError("Video Capture failed!");

        if (QuitAfterSave)
            Application.Quit();

    }

    private void VideoCaptureCompleted()
    {
        Status = RecordingStatus.Ended;
        Debug.Log("Video was saved.");

        if (QuitAfterSave)
            Application.Quit();
    }

    // See if we can quit already, otherwise wait.
    private static bool WantsToQuit()
    {
        if (Instance.Status == RecordingStatus.Recording)
        {
            QuitAfterSave = true;
            Instance.StopRecording();
            return false;
        }

        return Instance.Status != RecordingStatus.FinishingUp;
    }

    void Update()
    {

    }

    void FixedUpdate()
    {

    }
    
    void LateUpdate()
    {
        if (Status == RecordingStatus.Recording)
        {
            Model.NewFrame(Time.time);

            foreach (var go in GameObjectRecordings)
            {
                //RecordedVariables R = go.gameObject.GetComponent<RecordedVariables>();
                RecVars recVars = go.recordedVars;
                int id = go.ID;
                //if (recVars != null)
                //{

                // Check for each recording flag if it was set and add to recording
                if (RecordingController.FlagsHelper.IsSet<RecordingController.RecVars>(recVars, RecordingController.RecVars.X))
                    Model.RecordVar(id++, go.gameObject.transform.position.x);
                if (RecordingController.FlagsHelper.IsSet<RecordingController.RecVars>(recVars, RecordingController.RecVars.Y))
                    Model.RecordVar(id++, go.gameObject.transform.position.y);
                if (RecordingController.FlagsHelper.IsSet<RecordingController.RecVars>(recVars, RecordingController.RecVars.Z))
                    Model.RecordVar(id++, go.gameObject.transform.position.z);
                if (RecordingController.FlagsHelper.IsSet<RecordingController.RecVars>(recVars, RecordingController.RecVars.RX))
                    Model.RecordVar(id++, go.gameObject.transform.rotation.x);
                if (RecordingController.FlagsHelper.IsSet<RecordingController.RecVars>(recVars, RecordingController.RecVars.RY))
                    Model.RecordVar(id++, go.gameObject.transform.rotation.y);
                if (RecordingController.FlagsHelper.IsSet<RecordingController.RecVars>(recVars, RecordingController.RecVars.RZ))
                    Model.RecordVar(id++, go.gameObject.transform.rotation.z);
                if (RecordingController.FlagsHelper.IsSet<RecordingController.RecVars>(recVars, RecordingController.RecVars.SX))
                    Model.RecordVar(id++, go.gameObject.transform.localScale.x);
                if (RecordingController.FlagsHelper.IsSet<RecordingController.RecVars>(recVars, RecordingController.RecVars.SY))
                    Model.RecordVar(id++, go.gameObject.transform.localScale.y);
                if (RecordingController.FlagsHelper.IsSet<RecordingController.RecVars>(recVars, RecordingController.RecVars.SZ))
                    Model.RecordVar(id++, go.gameObject.transform.localScale.z);
                
                //}
                //else
                //{
                //    Model.RecordVar(go.ID, go.gameObject.transform.position.x);
                //    Model.RecordVar(go.ID + 1, go.gameObject.transform.position.y);
                //    Model.RecordVar(go.ID + 2, go.gameObject.transform.position.z);
                //}


            }

            // Handle script variables
            foreach (var mb in MBRecordings)
            {
                // Check if Var is to be recorded at an interval
                if (mb.Attr.Interval > 0f)
                {
                    // Record only if next Interval reached
                    if (mb.NextRecordTime <= Time.time)
                    {
                        mb.NextRecordTime = Time.time + mb.Attr.Interval;
                    }
                    else
                    {
                        continue;
                    }
                }

                if (mb.GetType() == typeof(FieldRecording))
                {
                    var fr = mb as FieldRecording;
                    if (fr != null)
                    {
                        Model.RecordVar(fr.ID,
                            fr.fieldInfo.GetValue(fr.MonoBehavior));
                    }
                }
                else if (mb.GetType() == typeof(PropertyRecording))
                {
                    var pr = mb as PropertyRecording;
                    if (pr != null)
                    {
                        Model.RecordVar(pr.ID,
                            pr.propertyInfo.GetValue(pr.MonoBehavior, null));
                    }
                }
            }

            // Save data in intervals
            if (Time.time > nextSaveTime)
            {
                nextSaveTime = Time.time + 2f;
                Model.SaveToFile();
                if (Info.HasChanged)
                {
                    Model.SaveJSON(Info);
                    Info.HasChanged = false;
                }
            }
        }

    }

    /// <summary>
    /// Add a field of a monobehaviour script to the recording
    /// </summary>
    /// <param name="monoBehaviour"></param>
    /// <param name="fieldInfo"></param>
    /// <param name="attr"></param>
    public void AddFieldRecording(MonoBehaviour monoBehaviour, FieldInfo fieldInfo, RecordAttribute attr)
    {
        FieldRecording fr = new FieldRecording(monoBehaviour, fieldInfo, attr);
        MBRecordings.Add(fr);
        Info.AddColumn(fr.ID, fr.Name + "_" + monoBehaviour.GetType() + "_" + monoBehaviour.name + "_" + monoBehaviour.gameObject.GetInstanceID());
    }

    /// <summary>
    /// Add a property of a monobehaviour script to the recording
    /// </summary>
    /// <param name="monoBehaviour"></param>
    /// <param name="propertyInfo"></param>
    /// <param name="attr"></param>
    public void AddPropertyRecording(MonoBehaviour monoBehaviour, PropertyInfo propertyInfo, RecordAttribute attr)
    {
        PropertyRecording pr = new PropertyRecording(monoBehaviour, propertyInfo, attr);
        MBRecordings.Add(pr);
        Info.AddColumn(pr.ID, pr.Name + "_" + monoBehaviour.GetType() + "_" + monoBehaviour.name + "_" + monoBehaviour.gameObject.GetInstanceID());
    }

    /// <summary>
    /// Add a gameobject to the recording. This method only records the objects position.
    /// </summary>
    /// <param name="go"></param>
    public void AddGameObjectRecording(GameObject go)
    {
        GameObjectRecording gor = new GameObjectRecording(go, RecVars.X | RecVars.Y | RecVars.Z);
        GameObjectRecordings.Add(gor);
        //Info.AddColumn(gor.ID, gor.name + "_" + gor.gameObject.GetInstanceID());
        Info.AddGameObject(gor.ID, gor.Name + "_" + gor.gameObject.GetInstanceID());
    }

    /// <summary>
    /// Add a gameobject to the recording. Everything specified in the RecVars will be saved.
    /// </summary>
    /// <param name="go"></param>
    /// <param name="recVars"></param>
    public void AddGameObjectRecording(GameObject go, RecVars recVars)
    {
        GameObjectRecording gor = new GameObjectRecording(go, recVars);
        GameObjectRecordings.Add(gor);
        //Info.AddColumn(gor.ID, gor.name + "_" + gor.gameObject.GetInstanceID());
        Info.AddGameObject(gor.ID, gor.Name + "_" + gor.gameObject.GetInstanceID(), recVars);
    }
    
    /// <summary>
    /// Flag enum for selection recorded parameters
    /// </summary>
    [Flags]
    public enum RecVars
    {
        None = 0,
        X = 1,
        Y = 2,
        Z = 4,
        RX = 8,
        RY = 16,
        RZ = 32,
        SX = 64,
        SY = 128,
        SZ = 256
    }

    // Parts from https://stackoverflow.com/questions/3261451/using-a-bitmask-in-c-sharp
    public static class FlagsHelper
    {
        public static bool IsSet<T>(T flags, T flag) where T : struct
        {
            int flagsValue = (int)(object)flags;
            int flagValue = (int)(object)flag;

            return (flagsValue & flagValue) != 0;
        }

        public static void Set<T>(ref T flags, T flag) where T : struct
        {
            int flagsValue = (int)(object)flags;
            int flagValue = (int)(object)flag;

            flags = (T)(object)(flagsValue | flagValue);
        }

        public static void Unset<T>(ref T flags, T flag) where T : struct
        {
            int flagsValue = (int)(object)flags;
            int flagValue = (int)(object)flag;

            flags = (T)(object)(flagsValue & (~flagValue));
        }

        public static void Set<T>(ref T flags, T flag, bool set) where T : struct
        {
            if (set)
                Set<T>(ref flags, flag);
            else
                Unset<T>(ref flags, flag);
        }

        // Can be used since number of flags is small enough
        public static int GetSetBits(RecVars flags)
        {
            int v = (int)flags;
            int count = 0;

            while (v != 0)
            {
                v = v & (v - 1);
                count++;
            }

            return count;
        }
    }

    /// <summary>
    /// Base class for all recorded types
    /// </summary>
    public class ElementRecording
    {
        public int ID;
        public string Name;

        protected static int NextID = 2;

        public ElementRecording(string name)
        {
            ID = NextID++;
            this.Name = name;
        }
    }

    /// <summary>
    /// Keeps track of recorded gameobject parameters
    /// </summary>
    public class GameObjectRecording : ElementRecording
    {
        public GameObject gameObject;
        public RecVars recordedVars;

        public GameObjectRecording(GameObject go, RecVars recVars) : base(go.name)
        {
            gameObject = go;
            recordedVars = recVars;

            NextID += FlagsHelper.GetSetBits(recVars) - 1;

            Debug.Log(Name + " Flags = " + FlagsHelper.GetSetBits(recVars) + "ID = " + ID + " NEXT = " + NextID);
        }
    }

    /// <summary>
    /// Base class for recordings from monobehaviour variables
    /// </summary>
    public class MonoBehaviourRecording : ElementRecording
    {
        public MonoBehaviour MonoBehavior;
        public RecordAttribute Attr;
        public float NextRecordTime;

        public MonoBehaviourRecording(MonoBehaviour monoBehaviour, RecordAttribute attr) : base(attr.VarName)
        {
            this.MonoBehavior = monoBehaviour;
            this.Attr = attr;
            NextRecordTime = -1f;
        }
    }

    /// <summary>
    /// Class for saving information for recorded fields
    /// </summary>
    public class FieldRecording : MonoBehaviourRecording
    {

        public FieldInfo fieldInfo;

        public FieldRecording(MonoBehaviour monoBehaviour, FieldInfo fieldInfo, RecordAttribute attr) : base(monoBehaviour, attr)
        {
            this.fieldInfo = fieldInfo;
        }
    }

    /// <summary>
    /// Class for saving information for recorded properties
    /// </summary>
    public class PropertyRecording : MonoBehaviourRecording
    {
        public PropertyInfo propertyInfo;

        public PropertyRecording(MonoBehaviour monoBehaviour, PropertyInfo propertyInfo, RecordAttribute attr) : base(monoBehaviour, attr)
        {
            this.propertyInfo = propertyInfo;
        }
    }

    /// <summary>
    /// Enum with states of the recording
    /// </summary>
    private enum RecordingStatus
    {
        NotStarted, Recording, Ended, FinishingUp
    }
}
