﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using RockVR.Video;
using UnityEngine;

/// <summary>
/// Acquires fields, properties, gameobjects and VR devices and adds them to the Controller.
/// </summary>
[RequireComponent(typeof(RecordingController))]
public class MainRecorder : MonoBehaviour
{
    public GameObject[] RecordedGameObjects;
    //public List<RecordingController.RecVars> RecordedGameObjectsVars = new List<RecordingController.RecVars>();

    private RecordingController _controller;

    // VR Objects
    Transform headset, lController, rController;
   
    // Use this for initialization
    void Start ()
	{
	    _controller = GetComponent<RecordingController>();
        //FieldInfo[] fi = tracking.GetComponent<CubeMover>().GetType().GetFields();
        //foreach (var f in fi)
        //{
        //    Debug.Log(f.FieldType + " " + f.GetValue(tracking.GetComponent<CubeMover>()));
        //}

        FindVRDevices();

        FindObjectsWithRecordAttribute();
	    foreach (var go in RecordedGameObjects)
	    {
            var recVars = go.GetComponent<RecordedVariables>();
            if(recVars != null)
	            _controller.AddGameObjectRecording(go, recVars.recVars);
            else
                _controller.AddGameObjectRecording(go);
        }

        // TODO For Testing
        //StartCoroutine(StartRecordingSoon());
	}

    private IEnumerator StartRecordingSoon()
    {
        yield return new WaitForSeconds(3f);
        Debug.Log("Starting");
        _controller.StartRecording();
    }

    void OnApplicationQuit()
    {
        if(_controller != null)
            _controller.StopRecording();
    }

    private void FindObjectsWithRecordAttribute()
    {
        MonoBehaviour[] activeGameObjects = GameObject.FindObjectsOfType<MonoBehaviour>();

        foreach (var obj in activeGameObjects)
        {
            Type objType = obj.GetType();

            var objFields = objType.GetFields(BindingFlags.Instance | BindingFlags.Public);

            foreach (var fieldInfo in objFields)
            {
                RecordAttribute attr =
                    Attribute.GetCustomAttribute(fieldInfo, typeof(RecordAttribute)) as RecordAttribute;

                if (attr != null)
                {
                    Debug.Log(attr.VarName + " : " + obj.name + " : " + fieldInfo.GetValue(obj));
                    _controller.AddFieldRecording(obj, fieldInfo, attr);
                }
            }

            var objProperties = objType.GetProperties(BindingFlags.Instance | BindingFlags.Public);

            foreach (var propertyInfo in objProperties)
            {
                RecordAttribute attr =
                    Attribute.GetCustomAttribute(propertyInfo, typeof(RecordAttribute)) as RecordAttribute;

                if (attr != null)
                {
                    Debug.Log(attr.VarName + " : " + obj.name + " : " + propertyInfo.GetValue(obj, null));
                    _controller.AddPropertyRecording(obj, propertyInfo, attr);
                }
            }
        }
    }

    // Try to find missing VR devices
    private void FindVRDevices()
    {
        

        _controller.setVRObjects(headset, lController, rController);
    }

    // Update is called once per frame
    void Update ()
    {
        if (headset == null || lController == null || rController == null)
            FindVRDevices();
    }
}
