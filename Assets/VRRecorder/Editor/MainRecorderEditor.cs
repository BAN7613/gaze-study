﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

/// <summary>
/// Inspector GUI logic for the Main Recorder
/// </summary>
[CustomEditor(typeof(MainRecorder))]
public class MainRecorderEditor : Editor {

    List<bool> toggleGroups = new List<bool>();
    bool px = false;
    bool enabled = true;

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        var script = target as MainRecorder;

        UpdateRecVars(script);

        var n = script.RecordedGameObjects.Length;
        while (toggleGroups.Count < n)
            toggleGroups.Add(false);

        while (toggleGroups.Count > n)
            toggleGroups.RemoveAt(-1);

        int index = 0;
        foreach (GameObject go in script.RecordedGameObjects)
        {
            toggleGroups[index] = EditorGUILayout.BeginToggleGroup("Show Settings for: " + go.name, toggleGroups[index]);
            if (toggleGroups[index])
            {
                var r = go.GetComponent<RecordedVariables>();
                bool b;
                string name;
                foreach(RecordingController.RecVars v in Enum.GetValues(typeof(RecordingController.RecVars))){
                    if (RecordingController.RecVars.None == v)
                        continue;

                    name = Enum.GetName(typeof(RecordingController.RecVars), v);
                    b = RecordingController.FlagsHelper.IsSet<RecordingController.RecVars>(r.recVars, v);
                    RecordingController.FlagsHelper.Set<RecordingController.RecVars>(ref r.recVars, v, EditorGUILayout.Toggle("" + name, b));
                }

                EditorGUILayout.LabelField("Recorded Vars #" + RecordingController.FlagsHelper.GetSetBits(r.recVars));
            }
            EditorGUILayout.EndToggleGroup();

            index++;
        }
    }

    private void UpdateRecVars(MainRecorder script)
    {
        foreach(GameObject go in script.RecordedGameObjects)
        {
            var r = go.GetComponent<RecordedVariables>();
            if(r == null)
            {
                go.AddComponent<RecordedVariables>();
            }
        }
    }
}
