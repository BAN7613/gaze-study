﻿using System;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.XR.WSA.Persistence;

/// <summary>
/// Represents JSON data of experiment meta-data
/// </summary>
public class RecordingInfo
{
    public DateTime _date;
    public Dictionary<int, string> _columnNames;
    public Dictionary<string, object> _overviewCamera;
    public Dictionary<string, string> _settings;

    [JsonIgnore]
    public bool HasChanged = false;

    public RecordingInfo(DateTime date)
    {
        _date = date;
        _columnNames = new Dictionary<int, string>();
        _overviewCamera = new Dictionary<string, object>();
        _settings = new Dictionary<string, string>();
    }

    public void AddColumn(int id, string name)
    {
        _columnNames.Add(id, name);
        HasChanged = true;
    }

    internal void AddGameObject(int id, string name)
    {
        _columnNames.Add(id, name + ".x");
        _columnNames.Add(id + 1, name + ".y");
        _columnNames.Add(id + 2, name + ".z");
        HasChanged = true;
    }

    internal void AddGameObject(int id, string name, RecordingController.RecVars recVars)
    {
        name = name.Replace(".", "_");
        if(RecordingController.FlagsHelper.IsSet<RecordingController.RecVars>(recVars, RecordingController.RecVars.X))
            _columnNames.Add(id++, name + ".x");
        if (RecordingController.FlagsHelper.IsSet<RecordingController.RecVars>(recVars, RecordingController.RecVars.Y))
            _columnNames.Add(id++, name + ".y");
        if (RecordingController.FlagsHelper.IsSet<RecordingController.RecVars>(recVars, RecordingController.RecVars.Z))
            _columnNames.Add(id++, name + ".z");
        if (RecordingController.FlagsHelper.IsSet<RecordingController.RecVars>(recVars, RecordingController.RecVars.RX))
            _columnNames.Add(id++, name + ".rx");
        if (RecordingController.FlagsHelper.IsSet<RecordingController.RecVars>(recVars, RecordingController.RecVars.RY))
            _columnNames.Add(id++, name + ".ry");
        if (RecordingController.FlagsHelper.IsSet<RecordingController.RecVars>(recVars, RecordingController.RecVars.RZ))
            _columnNames.Add(id++, name + ".rz");
        if (RecordingController.FlagsHelper.IsSet<RecordingController.RecVars>(recVars, RecordingController.RecVars.SX))
            _columnNames.Add(id++, name + ".sx");
        if (RecordingController.FlagsHelper.IsSet<RecordingController.RecVars>(recVars, RecordingController.RecVars.SY))
            _columnNames.Add(id++, name + ".sy");
        if (RecordingController.FlagsHelper.IsSet<RecordingController.RecVars>(recVars, RecordingController.RecVars.SZ))
            _columnNames.Add(id++, name + ".sz");
        HasChanged = true;
    }

    public void AddSetting(string key, string value)
    {
        _settings.Add(key, value);
        HasChanged = true;
    }

    public void AddOverviewCamera(Camera cam, byte[] pngData)
    {
        if (!cam.orthographic)
        {
            Debug.LogError("Screenshot / Overview Camera must be orthographic!");
            cam.orthographic = true;
        }

        Vector3 v = cam.ViewportToWorldPoint(new Vector3(0, 0, cam.nearClipPlane));
        _overviewCamera.Add("00", v);
        v = cam.ViewportToWorldPoint(new Vector3(0, 1, cam.nearClipPlane));
        _overviewCamera.Add("01", v);
        v = cam.ViewportToWorldPoint(new Vector3(1, 0, cam.nearClipPlane));
        _overviewCamera.Add("10", v);
        v = cam.ViewportToWorldPoint(new Vector3(1, 1, cam.nearClipPlane));
        _overviewCamera.Add("11", v);

        _overviewCamera.Add("Base64PNG", pngData);

        cam.gameObject.SetActive(false);
        HasChanged = true;
    }
}
