﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RecordedVariables : MonoBehaviour {

    [HideInInspector]
    public RecordingController.RecVars recVars = RecordingController.RecVars.X | RecordingController.RecVars.Y | RecordingController.RecVars.Z;

}
