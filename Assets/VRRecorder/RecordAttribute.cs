﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Used to specify information for the variables that need to be captured.
/// Can be added as an Attribute to fields and properties in other classes.
/// </summary>
[AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
public class RecordAttribute : Attribute
{
    // Name of var in saved data
    public string VarName { get; set; }

    // Interval in seconds in which to save var (max is dependant on Unity)
    public float Interval { get; set; }

    /// <summary>
    /// For testing.
    /// You should specify a name using the other constructors.
    /// </summary>
    public RecordAttribute()
    {

    }

    public RecordAttribute(string varName)
    {
        VarName = varName;
        Interval = 0f; // Max rate
    }

    public RecordAttribute(string varName, float interval)
    {
        VarName = varName;
        Interval = interval;
    }


}
