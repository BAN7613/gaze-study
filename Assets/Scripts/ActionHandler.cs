﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class ActionHandler : MonoBehaviour
{
    public static ActionHandler instance;

    public SteamVR_Action_Boolean GrabbingBall;  //reference to the SteamVR2.0 Action Binding on Trigger Button
    public SteamVR_Action_Boolean NextRound;    //reference to the SteamVR2.0 Action Reference - TargetActivation
    public SteamVR_Input_Sources handType;       // reference to Hand 
    public SteamVR_Behaviour_Pose trackedObject; // reference to Behaviour in SteamVR2.0
    public VelocityEstimator velocityEst;       //reference to VelocityEstimator Script
    public GameObject Ball;                      // Reference to Ball Game Obj
    public GameObject BallSnapPos;              //Reference to Ball Snapping position on Controller
    public Vector3 initBallPos;                 //reference where the ball should spawn
    public Vector3 lockedGazePoint;             //reference to the detected Fixation of user with the Eyetracker



    private Boolean ballInHand;                 //Ref. wether the Ball is in the Users Hand
    public bool touchpressed;                   //Marker when the Touchpad was triggered - Marker when the Target was Activated
    private Vector3 _currentGrabbedLocation;
    private GameObject collidingObject;         //reference to the Controller/Ball Obj for Collision detection to pick up the Ball

    public string SysTimeTriggerpress;          //Timestamp when trigger was pressed
    public string SysTimeTriggerRelease;        //Timestamp when trigger was released
    public string SysTimeTargetAct;
    [Record("isTriggerpressed")]                //Time when Target was activated
    public bool isTriggerpressed;               //Bool wether trigger is currently pressed
    public bool targetActButtonPress;           //Bool wheter touch pad for target act. is pressed

    public Vector3 _velocityMotorThrow;         //Vector to hold the Velocity measured with the controllers movement (Velocity for motor Throw)

    public int ballsThrown;                     //Counter how many balls have been thrown




    private void Awake()                                //Awake Method called before start
    {
        if (instance != null && instance != this)       //Check if instance of this script already is loaded
        {
            Destroy(this.gameObject);                       //if so, destroy this Obj.
        }
        else
        {
            instance = this;                            //else the instance is this Obj.
        }                                               //This Part helps referencing this script with the call instance.XXXXX
    }

    // Start is called before the first frame update
    void Start()
    {
        GrabbingBall.AddOnStateDownListener(TriggerDown, handType);     //Add the listeners to the Buttons of the Controller
        GrabbingBall.AddOnStateUpListener(TriggerUp, handType);
        NextRound.AddOnStateDownListener(TouchPress, handType);
        trackedObject = this.gameObject.GetComponent<SteamVR_Behaviour_Pose>();     //saving controller reference from SteamVR in trackedObj
        velocityEst = this.gameObject.GetComponent<VelocityEstimator>();            //saving reference to velocity estimator
        _currentGrabbedLocation = new Vector3();                                    //creating new vector for current grabed Location

        ballInHand = false;                                                         //instancing the initial Bool Values

        touchpressed = false;                                                       //instancing the initial Bool Values
        isTriggerpressed = false;                                                   //instancing the initial Bool Values
        targetActButtonPress = false;                                               //instancing the initial Bool Values
        ballsThrown = 0;                                                            //instancing the initial throw counter
    }


    // Update is called once per frame
    void Update()
    {

    }

    public void TriggerUp(SteamVR_Action_Boolean fromAction, SteamVR_Input_Sources fromSource)          //when Trigger gets released this method gets called
    {

        isTriggerpressed = false;                                                                       //set status Bool
        if (collidingObject != null && ballInHand == true)                                              //when ball is in Hand, do
        {

            Vector3 velocity = velocityEst.GetVelocityEstimate();                                       //get the estimated and smoothed velocity of the Controller
            _velocityMotorThrow = velocity;                                                             //save it in BPublic Var
            initBallPos = Ball.transform.position;                                                      //save pos where Ball got released


            GazeAssist.instance.spawnGazeTargetMarker();                                                //spwan visual representation of weighted Ball target
            GazeAssist.instance.spawnTobiiPoint();                                                      //spawn visual representation of Eyetracking fixation

            ballsThrown += 1;                                                                           //incr. throws

            Vector3 velocityAngu = velocityEst.GetAngularVelocityEstimate();                            //save smoothed angular velocity of throw behaviour


            lockedGazePoint = GazeAssist.instance._focusedGazeHitPoint;                                 //get fixated Target Point
            ReleaseGameObj(ConditionHandler());                                                         //Release BallMethod called with value of condition
            SysTimeTriggerRelease = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds().ToString();          //Timestamp TriggerRelease
            ballInHand = false;                                                                         //bool setting
        }


    }

    public void TriggerDown(SteamVR_Action_Boolean fromAction, SteamVR_Input_Sources fromSource)            //gets called when trigger gets pressed down
    {
        isTriggerpressed = true;                                                                            //update bools

        if (collidingObject != null)                                                                        //if controller collides with ball                                                               
        {
            HoldGameObj(collidingObject);                                                                   //call Hold Ball Method
            SysTimeTriggerpress = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds().ToString();                //Timestamp saved when trigger got pressed

            // Activate target on pickup
            TouchPress(fromAction, fromSource);
        }
        else
        {
            // Check if Ball is close
            var dist = Vector3.Distance(BallSnapPos.transform.position, Ball.transform.position);
            
            // If less than 10cm to Ball, attach it anyway.
            if(dist <= 0.10f)
            {
                collidingObject = Ball;
                HoldGameObj(collidingObject);                                                                   //call Hold Ball Method
                SysTimeTriggerpress = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds().ToString();                //Timestamp saved when trigger got pressed

                // Activate target on pickup
                TouchPress(fromAction, fromSource);
            }
        }
    }

    public void TouchPress(SteamVR_Action_Boolean fromAction, SteamVR_Input_Sources fromSource)             //when touchpad gets clicked, this method gets called
    {

        if (Ballmanager.instance.ready4nextRound)                                                           //when system is ready for next round do...
        {
            targetActButtonPress = true;
            MaterialManager.instance.ChangeColorMaterial(GameManager.instance.ActiveTargetNr);              //set status bools, activate the coloured version of Material
            Ballmanager.instance.ready4nextRound = false;
        }
    }

    private void SetCollidingObject(Collider col)
    {
        if (collidingObject || !col.GetComponent<Rigidbody>())
        {
            return;
        }
        collidingObject = col.gameObject;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<Throwable>() || other.tag == "Ball")                                //find stable collision with only the Ball object, with Tag Ball
        {
            SetCollidingObject(other);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.GetComponent<Throwable>() || other.tag == "Ball")
        {
            SetCollidingObject(other);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (!collidingObject)
        {
            return;
        }
        collidingObject = null;
    }

    public void HoldGameObj(GameObject throwableObj)                                                //Method that handles holding the Ball at the Controller
    {
        if (throwableObj != null)
        {
            throwableObj.transform.parent = this.gameObject.transform;                                //create Parent child relationship between ball & controller
            collidingObject.GetComponent<Rigidbody>().isKinematic = true;                              //kinematic of Ball gets activatd
            collidingObject.transform.position = BallSnapPos.transform.position;                        //set Position of ball to snapping Pos.
            ballInHand = true;                                                                          //Status bool
        }
    }

    public void ReleaseGameObj(Vector3 ReleaseVelocity)                                             //Method that handles releasing the Ball
    {
        collidingObject.transform.parent = null;                                                      //destroy parent relationship
        Rigidbody rigidbody = collidingObject.GetComponent<Rigidbody>();                              //get rigidbody of ball to apply velocities
        rigidbody.isKinematic = false;                                                                 //disable kinematic
        rigidbody.velocity = ReleaseVelocity;                                                         //Apply calculated release velocity, that will fly the ball in the desired Target point.
        Debug.Log($"Release velocity: {ReleaseVelocity}");
        collidingObject = null;
        ballInHand = false;
    }

    public Vector3 GazeAssistCalc(Vector3 Target)                                                       //Method that handels calculation triggering for Release Velocity
    {
        var initBallVelo = collidingObject.GetComponent<Rigidbody>().velocity;                          //get initial velocity of Ball
        Vector3 Velocity = GazeAssist.instance.CalcVelII(Target);                                       //Call Method, that simulates Trajectory into target Point 
        return Velocity;                                                                                //Return needed Velocity for Throw into desired Target Point
    }

    public Vector3 ConditionHandler()                                                                   //This Method handles setting the right Ball Weighting between Gaze Point and Motor
                                                                                                        // Point depending on the Set Condition in the Game Manager and the Nr. of throws
    {
        switch (GameManager.instance.condition)
        {
            case GameManager.Condition.Motor:
                return _velocityMotorThrow;

            case GameManager.Condition.Gaze:
                var GazeVelo = GazeAssistCalc(GazeAssist.instance._focusedGazeHitPoint);
                return GazeVelo;

            case GameManager.Condition.Motor2Gaze:
                if (GameManager.instance.ThrowCounter <= 5)
                {
                    GazeAssist.instance._gazeWeighting = 1f;
                }
                else if (GameManager.instance.ThrowCounter <= 10)
                {
                    GazeAssist.instance._gazeWeighting = 0.8f;
                }
                else if (GameManager.instance.ThrowCounter <= 15)
                {
                    GazeAssist.instance._gazeWeighting = 0.6f;
                }
                else if (GameManager.instance.ThrowCounter <= 20)
                {
                    GazeAssist.instance._gazeWeighting = 0.4f;
                }
                else if (GameManager.instance.ThrowCounter <= 25)
                {
                    GazeAssist.instance._gazeWeighting = 0.2f;
                }
                else if (GameManager.instance.ThrowCounter <= 30)
                {
                    GazeAssist.instance._gazeWeighting = 0f;
                }
                var target = GazeAssist.instance.getweightetGazeBullseyeV3(GazeAssist.instance._focusedGazeHitPoint, GazeAssist.instance._motorThrowHitPoint);
                var M2G = GazeAssistCalc(target);
                return M2G;

            case GameManager.Condition.Gaze2Motor:
                if (GameManager.instance.ThrowCounter <= 5)
                {
                    GazeAssist.instance._gazeWeighting = 0f;
                }
                else if (GameManager.instance.ThrowCounter <= 10)
                {
                    GazeAssist.instance._gazeWeighting = 0.2f;
                }
                else if (GameManager.instance.ThrowCounter <= 15)
                {
                    GazeAssist.instance._gazeWeighting = 0.4f;
                }
                else if (GameManager.instance.ThrowCounter <= 20)
                {
                    GazeAssist.instance._gazeWeighting = 0.6f;
                }
                else if (GameManager.instance.ThrowCounter <= 25)
                {
                    GazeAssist.instance._gazeWeighting = 0.8f;
                }
                else if (GameManager.instance.ThrowCounter <= 30)
                {
                    GazeAssist.instance._gazeWeighting = 1f;
                }
                var targeta = GazeAssist.instance.getweightetGazeBullseyeV3(GazeAssist.instance._focusedGazeHitPoint, GazeAssist.instance._motorThrowHitPoint);
                var G2M = GazeAssistCalc(targeta);
                return G2M;

            case GameManager.Condition.BullsEye2Motor:
                if (GameManager.instance.ThrowCounter <= 5)
                {
                    GazeAssist.instance._gazeWeighting = 0f;
                }
                else if (GameManager.instance.ThrowCounter <= 10)
                {
                    GazeAssist.instance._gazeWeighting = 0.2f;
                }
                else if (GameManager.instance.ThrowCounter <= 15)
                {
                    GazeAssist.instance._gazeWeighting = 0.4f;
                }
                else if (GameManager.instance.ThrowCounter <= 20)
                {
                    GazeAssist.instance._gazeWeighting = 0.6f;
                }
                else if (GameManager.instance.ThrowCounter <= 25)
                {
                    GazeAssist.instance._gazeWeighting = 0.8f;
                }
                else if (GameManager.instance.ThrowCounter <= 30)
                {
                    GazeAssist.instance._gazeWeighting = 1f;
                }

                Vector3 ActiveBullsEye;

                switch (GameManager.instance.ActiveTargetNr)
                {
                    case 0:
                        ActiveBullsEye = ScoreCalc.instance.BullsEyeLeft.transform.position; // LEFT TARGET INDEX 0
                        break;

                    case 1:
                        ActiveBullsEye = ScoreCalc.instance.BullsEyeMiddle.transform.position; // LEFT TARGET INDEX 0

                        break;
                    case 2:
                        ActiveBullsEye = ScoreCalc.instance.BullsEyeRight.transform.position; // LEFT TARGET INDEX 0

                        break;
                    default:
                        ActiveBullsEye = new Vector3(0, 0, 0);
                        break;
                }

                var targetb = GazeAssist.instance.getweightetGazeBullseyeV3(ActiveBullsEye, GazeAssist.instance._motorThrowHitPoint);
                var B2M = GazeAssistCalc(targetb);
                return B2M;

            case GameManager.Condition.Motor2BullsEye:

                if (GameManager.instance.ThrowCounter <= 5)
                {
                    GazeAssist.instance._gazeWeighting = 1f;
                }
                else if (GameManager.instance.ThrowCounter <= 10)
                {
                    GazeAssist.instance._gazeWeighting = 0.8f;
                }
                else if (GameManager.instance.ThrowCounter <= 15)
                {
                    GazeAssist.instance._gazeWeighting = 0.6f;
                }
                else if (GameManager.instance.ThrowCounter <= 20)
                {
                    GazeAssist.instance._gazeWeighting = 0.4f;
                }
                else if (GameManager.instance.ThrowCounter <= 25)
                {
                    GazeAssist.instance._gazeWeighting = 0.2f;
                }
                else if (GameManager.instance.ThrowCounter <= 30)
                {
                    GazeAssist.instance._gazeWeighting = 0f;
                }

                Vector3 ActiveBullsEye1;

                switch (GameManager.instance.ActiveTargetNr)
                {
                    case 0:
                        ActiveBullsEye1 = ScoreCalc.instance.BullsEyeLeft.transform.position; // LEFT TARGET INDEX 0
                        break;

                    case 1:
                        ActiveBullsEye1 = ScoreCalc.instance.BullsEyeMiddle.transform.position; // LEFT TARGET INDEX 0

                        break;
                    case 2:
                        ActiveBullsEye1 = ScoreCalc.instance.BullsEyeRight.transform.position; // LEFT TARGET INDEX 0

                        break;
                    default:
                        ActiveBullsEye1 = new Vector3(0, 0, 0);
                        break;
                }

                var targetc = GazeAssist.instance.getweightetGazeBullseyeV3(ActiveBullsEye1, GazeAssist.instance._motorThrowHitPoint);
                var M2B = GazeAssistCalc(targetc);
                return M2B;

        }
        return _velocityMotorThrow;
    }
}


