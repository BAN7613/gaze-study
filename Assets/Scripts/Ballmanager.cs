﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class Ballmanager : MonoBehaviour
{
    public static Ballmanager instance;
    private int ballToRemove;
    public GameObject BallSpawnPos;                                         //Creating the needed Public Fields Spawn Pos. of Ball, Reference to collider of Ball, 
                                                                            //Ground Collider Ref, Explosion Animation....
    public Collider BallCollider;
    public Collider GroundCollider;
    public ParticleSystem Explosion;

    public bool Ballhit;

    public bool ready4nextRound;
    private System.Random rndm;

    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }
        else                                                            //initialize fields, make this script the only instance or destroy if another instance already exists
        {
            instance = this;
        }
        ready4nextRound = false;
        Ballhit = false;
        rndm = new System.Random();                                     //save random Number for random Target Activation
    }



    private void OnTriggerEnter(Collider Ball)                           //This Method handles wether the Ball hit the Target or Ground Plate
    {
        if (Ball.tag == "Ball")
        {
            Ballhit = true;
            BallCollider = Ball;
            GameManager.instance.BallhitTarget();                            //Trigger Explosion Animation
            Explode(Ball.transform.position);
            GetComponent<BoxCollider>().enabled = false;
            if (Ball.attachedRigidbody != null)
                Ball.attachedRigidbody.isKinematic = true;
        }
    }

    public IEnumerator ResetBall(Collider Ball)                            //Method that Resets Ball Position to Spawn
    {
        if (Ball.tag == "Ball")
        {
            Debug.Log("ResetBall meth. Aufgerufen");
            if (Ball.attachedRigidbody != null)
            {
                Ball.attachedRigidbody.velocity = Vector3.zero;                     //set Movement of Ball to zero
                Ball.attachedRigidbody.angularVelocity = Vector3.zero;
            }

            //yield return new WaitForSeconds(0.5f);
            var x = rndm.Next(0, 3);
            while (GameManager.instance.oldTarg == x)                       //Async Call for another random Target Nr for next round after Reset
            {
                x = rndm.Next(0, 3);
            }
            GameManager.instance.ActiveTargetNr = x;
            GameManager.instance.IncrementThrowCounter(1);
            EyeTracking_GazeDetect.instance.clearAveragers();
            Ballhit = false;
            Ball.transform.position = BallSpawnPos.transform.position;          //Set Position of Ball to Spawn
            if (Ball.attachedRigidbody != null)
                Ball.attachedRigidbody.isKinematic = false;
            GetComponent<BoxCollider>().enabled = true;
            GroundCollider.enabled = true;
            MaterialManager.instance.DeactivateTarg();                           //deactevate activated Target

            ready4nextRound = true;

            yield return null;
        }
    }

    void Explode(Vector3 Position)
    {
        var exp = Explosion;
        exp.transform.position = Position;                                  //Play Animation at Position
        exp.Play();
    }
}