﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tobii.G2OM;
using Tobii.XR;


public class EyeTracking_GazeDetect : MonoBehaviour
{
    static public EyeTracking_GazeDetect instance;

    public Vector3 GazeOrigin;
    public Vector3 GazeDirection;
    public GameObject TargetCenterPointleft;
    public GameObject TargetCenterPointmiddle;                  //References to The center Points of the BullsEyes
    public GameObject TargetCenterPointright;
    public GameObject Ball;                                     //Ref to Ball

    public List<Vector3> GazetargetFixations;                   //List of fixated Points in scene
    public List<float> GazeDists;                               //List of Distances to Bulls Eye
    private Vector3 summedGazeVec;                              
    private float sGD;
    public Vector3 avgV3;
    public float finalaverageGazeDist;
    public float gazedist;


    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
        }
    }

    // Update is called once per frame
    void Update()
    {
        var gazeRay = TobiiXR.EyeTrackingData.GazeRay;              //get Eyetracker Ray for Every Frame


        if (gazeRay.IsValid)                                            //when gaze is Valid do..
        {
            // Eye Origin is in World Space
            GazeOrigin = TobiiXR.EyeTrackingData.GazeRay.Origin;            

            // Eye Direction is a normalized direction vector in World Space
            GazeDirection = TobiiXR.EyeTrackingData.GazeRay.Direction;
            if (ActionHandler.instance.isTriggerpressed && ActionHandler.instance.targetActButtonPress) //save Focus Points only when Ball is in Hand and Target is Active
            {
                GazetargetFixations.Add(GazeRayHitPoint(GazeOrigin, GazeDirection));
                GazeDists.Add(GazeToTargetDist(GameManager.instance.ActiveTargetNr));
            }
        }
    }

    public Vector3 GazeRayHitPoint(Vector3 Origin, Vector3 Dircetion)                       //Raycast Pixel Exact hit Point of Eyetracking Gaze
    {
        Physics.Raycast(Origin, Dircetion, out RaycastHit hit, Mathf.Infinity, 1 << LayerMask.NameToLayer("GazeDetection"));
        return hit.point;
    }

    public Vector3 resolveTargetIDtoV3(int targetNr)                        //Translate Target ID to Vector to Center Point
    {
       if (targetNr == 0)
        { 
                //left
                Vector3 lpoint = TargetCenterPointleft.transform.position;
                return lpoint;
        }
        else if (targetNr == 1)
        {
            //middle
            Vector3 mpoint = TargetCenterPointmiddle.transform.position;
            return mpoint;
        }
        else if (targetNr == 2)
        { 
                //"right
                Vector3 rpoint = TargetCenterPointright.transform.position;
                return rpoint;
        }
       else
        {
            Vector3 end = new Vector3(0f, 0f, 0f);
            return end;
        }          
    }


    public float GazeToTargetDist(int TargetID)
    {
        float dist = Vector3.Distance(GazeRayHitPoint(GazeOrigin, GazeDirection), resolveTargetIDtoV3(TargetID));               //Calculate Distance between Gaze and Target
        return dist;
    }

    public float BallToTargetDist(int TargetID)
    {
        if(Ballmanager.instance.Ballhit)
        {
            float dist = Vector3.Distance(Ball.transform.position, resolveTargetIDtoV3(TargetID));                               //Calculate Distance of Ball to Target center
            return dist;
        }
        else
        {
            return 0.0f;
        }
    }

    public Vector3 calcTargetGazePoint()                    //smooth gaze fixation Point
    {
        GazetargetFixations.Reverse();
        int count = GazetargetFixations.Count;
        count = count / 2;
       // count = 20;
        int i = 0;
        while ( i< count )
        {
            summedGazeVec = summedGazeVec + GazetargetFixations[i];
            i++;
        }
        Vector3 AveragedVec;
        
         AveragedVec = summedGazeVec / count;
        avgV3 = AveragedVec;
        return AveragedVec;

    }

    public void gazeAverager()                  //smooth gaze Distance also
    {
        GazeDists.Reverse();
        int count = GazeDists.Count;
        count = count / 2;
        int i = 0;
        while (i < count)
        {
            sGD = sGD + GazeDists[i];
            i++;
        }
        finalaverageGazeDist = sGD / count;
    }

    public void clearAveragers()                    //clear the averager Variables
    {
       GazetargetFixations.Clear();
        GazeDists.Clear();
       avgV3 = Vector3.zero;
        finalaverageGazeDist = 0f;
        sGD = 0;
        summedGazeVec = Vector3.zero;
    }

    public float Motor2TargetDist(Vector3 MotorHP, Vector3 TargetP)         //Calc distance of Motor Hit Point to Target
    {
        var M2TD = Vector3.Distance(MotorHP, TargetP);
        return M2TD;
    }

    public float Motor2GazeDist(Vector3 MotorHP, Vector3 GazeHP)                //Calc Distance of Gaze to Motor hit point
    {
        var M2GD = Vector3.Distance(MotorHP, GazeHP);
        return M2GD;
    }

}
