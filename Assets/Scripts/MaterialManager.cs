﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaterialManager : MonoBehaviour
{
    public static MaterialManager instance;
    public GameObject TargetLeft;
    public GameObject TargetRight;
    public GameObject TargetMiddle;
    public GameObject TargetPodest;

    public Material BWMaterial;
    public Material ColMaterial;

    private Renderer T1Renderer;
    private Renderer T2Renderer;  //References to Target Renderers
    private Renderer T3Renderer;

    public List<Renderer> TargetRenderer;

    private bool isTargActive;

    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
        }

        TargetRenderer = new List<Renderer>     //Create List of the Renderers
        {
            TargetLeft.GetComponent<Renderer>(),
            TargetMiddle.GetComponent<Renderer>(),
            TargetRight.GetComponent<Renderer>()
        };

        T1Renderer = TargetLeft.GetComponent<Renderer>();
        T2Renderer = TargetRight.GetComponent<Renderer>();
        T3Renderer = TargetMiddle.GetComponent<Renderer>();

        DeactivateTarg();           //On start make all Targets B/W

        isTargActive = false;
    }

    public void ChangeBWMaterial(int TargNr)            //Method, that changes material of certain target to BW
    {
       Renderer TargRenderer = TargetRenderer[TargNr];
        if (isTargActive == true)
        {
            TargRenderer.material = BWMaterial;
            isTargActive = false;
        }
        
    }

    public void ChangeColorMaterial(int TargNr)     //Method that changes Target Material by ID  to  Coloured
    {
        Renderer TargRenderer = TargetRenderer[TargNr];
        if (isTargActive == false)
        {
            TargRenderer.material = ColMaterial;
            isTargActive = true;
        }
        
    }


    public void NewMaterial4nextRound(int oldTargnr, int newTargNr)     //Next round behaviour of Targets.
    {
 
        DeactivateTarg();
        ChangeColorMaterial(newTargNr);
    }

    public void DeactivateTarg()            //Method to deactivate targets 
    {
        T1Renderer.material = BWMaterial;
        T2Renderer.material = BWMaterial;
        T3Renderer.material = BWMaterial;
        isTargActive = false;
    }

    public void printTargets(int i)         //Helper to print id of Target that gets activated.
    {
        Debug.Log(TargetRenderer[i]);
    }
}
