﻿using Tobii.G2OM;
using Tobii.XR;
using UnityEngine;
using System.Text;
using System.IO;
using System.Collections.Generic;
using System;
using System.Collections;
using System.Threading;
using System.Threading.Tasks;



//Monobehaviour which implements the "IGazeFocusable" interface, meaning it will be called on when the object receives focus
public class MainStudy_Recorder : MonoBehaviour
{
    public static MainStudy_Recorder instance;

    //string FilePath;
    public List<string> EyeTrackingDataBuffer = new List<string>();
    
    [Record("rayOrigin")]
    public Vector3 rayOrigin = new Vector3();
    [Record("rayDirection")]
    public Vector3 rayDirection = new Vector3();
    [Record("screenPosition")]
    public Vector3 screenPosition = new Vector3();


    public Camera cam;
    public GameObject Ball;
    public bool LogEye2Console;


    public int interval = 200;

    private int counter = 0;
    public int ScoreperRound = 0;
    private string dateTimeFileName;
    private string FilePath;

    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
        }

    }

    private void Start() //Initialize File Name and Path of Logging File.
    {
        dateTimeFileName = "Main-" + DateTime.Now.ToString("MM/dd/yyyy hh-mm tt");
        dateTimeFileName = dateTimeFileName.Replace("/", "-");
        FilePath = Application.dataPath;
        FilePath = FilePath + "/Data/Main";
        Directory.CreateDirectory(FilePath);
        Debug.Log("The EyeTracking Data will be Saved @:" + FilePath);

    }



    async void WriteCSV(List<string> Buffer, string FileName) //Write the build string into the CSV to save the Data with an asynchronous Stream Writer.
    {

        string fn = Path.Combine(FilePath, FileName + ".csv");

        using (FileStream fs = new FileStream(fn, FileMode.Append, FileAccess.Write, FileShare.ReadWrite))
        using (StreamWriter sw = new StreamWriter(fs))
        {
            if (counter == 0)
            {

                await sw.WriteLineAsync("UnixTime;GameTime;Cond;usedHand;BallsThrown;Round;Weight;Target;TargetActive?;GazeValid?;TriggerState;BallonTarget?;LockedGazetoTargDist;Gaze2Targ Dist;Ball2Targ Dist; Motor2Targ Dist;Motor2Gaze Dist;" +
                    "PointsMotor;ScorethisRound;TotalScore;MotorHitPoint X;Y;Z; Ball X;Y;Z;LockedGazePointX;Y;Z;GazeHitPointX;Y;Z;rayOriginX;Y;Z;rayDirectionX;Y;Z");


                await sw.WriteLineAsync("SubjectID: " + RecordingController.Instance.SubjectID.ToString() + ";" + "ExperimentName: " + RecordingController.Instance.ExperimentName.ToString());
                counter += 1;
            }
            foreach (var item in Buffer)
            {
                await sw.WriteLineAsync(item);
            }
            sw.Flush();
        }
    }


    public string CaptureEyeTrackingData() //Colllect all necessary Data Points from other Scripts ect.
    {

        rayOrigin = TobiiXR.EyeTrackingData.GazeRay.Origin;
        rayDirection = TobiiXR.EyeTrackingData.GazeRay.Direction;
        string SystemTime = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds().ToString();
        string GameTime = Time.time.ToString();
        string rayOriginStr = V3toString(rayOrigin);
        string rayDirectionStr = V3toString(rayDirection);
        string gazeValidity = TobiiXR.EyeTrackingData.GazeRay.IsValid.ToString();
        string GazeRayhitPoint = V3toString(EyeTracking_GazeDetect.instance.GazeRayHitPoint(TobiiXR.EyeTrackingData.GazeRay.Origin, TobiiXR.EyeTrackingData.GazeRay.Direction));
        string GazeTargetDist = EyeTracking_GazeDetect.instance.GazeToTargetDist(GameManager.instance.ActiveTargetNr).ToString();
        string BallTargetDist = EyeTracking_GazeDetect.instance.BallToTargetDist(GameManager.instance.ActiveTargetNr).ToString();
        string MotorTargetDist = dist(GazeAssist.instance._motorThrowHitPoint, EyeTracking_GazeDetect.instance.resolveTargetIDtoV3(GameManager.instance.ActiveTargetNr));
        string MotorGazeDist = dist(GazeAssist.instance._focusedGazeHitPoint, GazeAssist.instance._motorThrowHitPoint);
        string MotorHitPoint = V3toString(GazeAssist.instance._motorThrowHitPoint);
        string weight = GazeAssist.instance._gazeWeighting.ToString();
        string actualPoints = calcMotorScore(GazeAssist.instance._motorThrowHitPoint).ToString();
        string scoredPoints = ScoreperRound.ToString();
        string totalScore = ScoreCalc.instance.TotalScore.ToString();
        string condition = GameManager.instance.condition.ToString();
        string hand = GameManager.instance.hand.ToString();

        string round = GameManager.instance.ThrowCounter.ToString();
        string ActTarget = resolveTargetID(GameManager.instance.ActiveTargetNr);
        string tarActbool = ActionHandler.instance.targetActButtonPress.ToString();
        string ballsThrown = ActionHandler.instance.ballsThrown.ToString();
        string triggerstate = ActionHandler.instance.isTriggerpressed.ToString();
        string BallPos = V3toString(Ball.transform.position);
        string BallState = Ballmanager.instance.Ballhit.ToString();
        string lockedGazePoint = V3toString(ActionHandler.instance.lockedGazePoint);
        string lockedGazeDisttoTarg = dist(ActionHandler.instance.lockedGazePoint, EyeTracking_GazeDetect.instance.resolveTargetIDtoV3(GameManager.instance.ActiveTargetNr));

        string FrameDataBuffer = $"{SystemTime};{GameTime};{condition};{hand};{ballsThrown};{round};{weight};{ActTarget};{tarActbool};{gazeValidity};{triggerstate};{BallState};" +
                                 $"{lockedGazeDisttoTarg};{GazeTargetDist};{BallTargetDist};{MotorTargetDist};{MotorGazeDist};{actualPoints};{ScoreperRound};{totalScore};" +
                                 $"{MotorHitPoint};{BallPos};{lockedGazePoint};{GazeRayhitPoint};{rayOriginStr};{rayDirectionStr};";

        if (LogEye2Console)
        {
            Debug.Log(FrameDataBuffer);
        }

        return FrameDataBuffer;
    }

    private string dist(Vector3 A, Vector3 B)  //Helper Method to calc distances between two Vectors
    {
        float dist = Vector3.Distance(A, B);
        return dist.ToString();
    }

    public string worldtoScreenCoord(Vector3 Origin, Vector3 Direction, float convergence, Camera cam) //Method for translate Eyetracking info to Screen Position 2 D Coordinates.
    {

        if (TobiiXR.EyeTrackingData.ConvergenceDistanceIsValid && TobiiXR.EyeTrackingData.GazeRay.IsValid)
        {
            Vector3 WorldPos = rayOrigin + rayDirection.normalized * convergence;
            screenPosition = cam.WorldToScreenPoint(WorldPos);
            string screenPositionStr = V3toString(screenPosition);
            return screenPositionStr;
        }
        else
        {
            string notValid = "NULL;NULL;NULL";
            return notValid;
        }
    }

    public string V3toString(Vector3 vec)           //Helper to build string from Vector
    {
        string formattedVec = (vec.x + ";" + vec.y + ";" + vec.z);
        return formattedVec;
    }

    void tempSaveEyeTrackingData()  //Tempsave Method that saves the data out of the Main Buffer.
    {

        List<string> TempList = new List<string>();
        foreach (var item in EyeTrackingDataBuffer)
        {
            TempList.Add(item);
        }

        EyeTrackingDataBuffer.Clear();
        Task.Factory.StartNew(() => WriteCSV(TempList, $"{dateTimeFileName.Replace(" ", "_")} Data_MAIN_"+ GameManager.instance.condition +"_Subj_" + RecordingController.Instance.SubjectID));
    }

    private void Update()       
    {

        EyeTrackingDataBuffer.Add(CaptureEyeTrackingData());//Call to  caputre Data every frame.

        if (Time.frameCount % interval == 0)
        {
            tempSaveEyeTrackingData();          //tempsave every Intervall set globally.

        }

    }

    public string CaptureStudyInfo(string Systime, string gameTime, string StartEndNewRound, int taregtID) 
    {
        string target = resolveTargetID(taregtID);
        string StudyInfo = $"{Systime};{gameTime};{StartEndNewRound};{target}";
        return StudyInfo;
    }

    public string resolveTargetID(int targetNr)
    {
        switch (targetNr)
        {
            case 0:
                string left = "left";
                return left;
            case 1:
                string middle = "middle";
                return middle;
            case 2:
                string right = "right";
                return right;
            case 99:
                string end = "end of Game";
                return end;
            default:
                string error = "ERROR couldnt resolve TargetID";
                return error;
        }
    }

    public int calcMotorScore(Vector3 MotorHitPoint)
    {
      var dist = ScoreCalc.instance.CalcDistancetoBullsEye(MotorHitPoint, GameManager.instance.ActiveTargetNr);
        return ScoreCalc.instance.DistanceToPoints(dist);
    }

    public void SaveTrackingData() //save the data collected.
    {
        WriteCSV(EyeTrackingDataBuffer, $"{dateTimeFileName.Replace(" ", "_")} Data_MAIN_" + GameManager.instance.condition + "_Subj_" + RecordingController.Instance.SubjectID);
    }

    void OnApplicationQuit()        //Before quitting Appl. Save the data.
    {
        SaveTrackingData();
        counter += 1;
    }
}