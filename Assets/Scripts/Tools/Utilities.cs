using System;
using UnityEngine;

namespace Helper
{
    public static class Utilities
    {
        public static Texture2D CaptureScreenshot(Camera captureCamera)
        {
            RenderTexture originalRenderTexture = RenderTexture.active;
            RenderTexture targetTexture = captureCamera.targetTexture;
            RenderTexture.active = targetTexture;

            Texture2D screenshot = new Texture2D(targetTexture.width, targetTexture.height);
            screenshot.ReadPixels(new Rect(0, 0, targetTexture.width, targetTexture.height), 0, 0);
            screenshot.Apply();

            // Replace the original active Render Texture.
            RenderTexture.active = originalRenderTexture;

            return screenshot;
        }

        public static Color GetColorWithAlphaTransition(Color color, float targetAlpha, float alphaTransition)
        {
            // targetAlpha is smaller than current alpha
            if (color.a > targetAlpha)
            {
                color.a = Math.Max(targetAlpha, color.a - alphaTransition);
            }
            // targetAlpha is greater than current alpha
            else
            {
                color.a = Math.Min(targetAlpha, color.a + alphaTransition);
            }
            return color;
        }

        public static float Map(this float value, float fromSource, float toSource, float fromTarget, float toTarget)
        {
            return (value - fromSource) / (toSource - fromSource) * (toTarget - fromTarget) + fromTarget;
        }

        public static void SetLocalScale(this Transform transform, float scaleX = 1f, float scaleY = 1f, float scaleZ = 1f)
        {
            Vector3 localScale = transform.localScale;
            localScale.Set(scaleX, scaleY, scaleZ);
            transform.localScale = localScale;
        }



        public static void DebugLogColor(string message, string color = "green")
        {
            Debug.Log($"<color='{color}'>{message}</color>");
        }
    }
}
