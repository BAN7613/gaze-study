﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VR_TrackDevice : MonoBehaviour
{
    // Unsere Tracker : F462F E1152
    // Einfach die letzten Zeichen vom Tracker im Inspector eingeben
    // In der Konsole werden beim Start alle S/N angezeigt
    public string[] DeviceSNs_End;
    public bool SmoothMovement;
    public float cameraSmoothingTime = 0.3f;

    private SteamTracker.VR_Device_Info deviceInfo;
    private Vector3 cameraButtonReferencePosition, targetPosition, velocity = Vector3.zero;

    // Start is called before the first frame update
    void Start()
    {
        SteamTracker._INSTANCE.OnNewDevice += OnNewDevice;
        SteamTracker._INSTANCE.OnNewPose += OnNewPose;
    }

    private void OnNewPose(SteamTracker.VR_Device_Info vr_device, Vector3 position, Quaternion rotation)
    {
        if(vr_device == deviceInfo)
        {
            if (!SmoothMovement)
            {
                transform.localPosition = position;
                transform.localRotation = rotation;
            }
            else
            {
                transform.localPosition = Vector3.SmoothDamp(transform.localPosition, position, ref velocity, cameraSmoothingTime);
                transform.localRotation =
                    Quaternion.Lerp(transform.localRotation, rotation, Time.deltaTime * (1 / cameraSmoothingTime));
            }
        }
    }

    private void OnNewDevice(SteamTracker.VR_Device_Info vr_device)
    {
        foreach (string DeviceSN_End in DeviceSNs_End)
        {
            if (vr_device.SerialNumber.EndsWith(DeviceSN_End))
            {
                deviceInfo = vr_device;
                break;
            }
        }

    }
}
