﻿using System.Collections;
using System.Collections.Generic;
using Helper;
using Valve.VR;
using UnityEngine;

public class SteamTracker : MonoBehaviour
{
    SteamVR_Events.Action newPosesAction;

    public delegate void NewDeviceEvent(VR_Device_Info vr_device);
    public event NewDeviceEvent OnNewDevice;

    public delegate void NewPoseEvent(VR_Device_Info vr_device, Vector3 position, Quaternion rotation);
    public event NewPoseEvent OnNewPose;

    private List<VR_Device_Info> trackedDevices;

    public class VR_Device_Info
    {
        readonly public SteamVR_TrackedObject.EIndex DeviceIndex;
        public string SerialNumber { get; internal set; }
        public string RenderModelName { get; internal set; }
        public string ModelNumber { get; internal set; }

        public VR_Device_Info(SteamVR_TrackedObject.EIndex deviceIndex, string serialNumber)
        {
            DeviceIndex = deviceIndex;
            SerialNumber = serialNumber;
        }

        public override bool Equals(object obj)
        {
            var info = obj as VR_Device_Info;
            return info != null &&
                   DeviceIndex == info.DeviceIndex;
        }

        public override int GetHashCode()
        {
            return 251616849 + DeviceIndex.GetHashCode();
        }

        public override string ToString()
        {
            return System.Enum.GetName(typeof(SteamVR_TrackedObject.EIndex), DeviceIndex) + " S/N: " + SerialNumber
                + $", RenderModelName: {RenderModelName},  ModelNumber: {ModelNumber}";
        }
    }

    public static SteamTracker _INSTANCE;

    void Awake()
    {
        if(_INSTANCE != null)
        {
            Destroy(this);
            return;
        }

        _INSTANCE = this;
        DontDestroyOnLoad(gameObject);
    }

    // Start is called before the first frame update
    void Start()
    {
        SteamVR.Initialize();
        newPosesAction = SteamVR_Events.NewPosesAction(OnNewPoses);
        newPosesAction.enabled = true;

        trackedDevices = new List<VR_Device_Info>();

        InvokeRepeating("FindTracketObjects", 0f, 3f);
    }

    private void FindTracketObjects()
    {
        foreach (SteamVR_TrackedObject.EIndex s in System.Enum.GetValues(typeof(SteamVR_TrackedObject.EIndex)))
        {
            var sn = GetDeviceInfo(s, ETrackedDeviceProperty.Prop_SerialNumber_String);
            VR_Device_Info device = new VR_Device_Info(s, sn);
            if (sn != null && !trackedDevices.Contains(device))
            {
                device.RenderModelName = GetDeviceInfo(s, ETrackedDeviceProperty.Prop_RenderModelName_String);
                device.ModelNumber = GetDeviceInfo(s, ETrackedDeviceProperty.Prop_ModelNumber_String);
                trackedDevices.Add(device);
                Utilities.DebugLogColor($"Connected VR Device: {device}", "orange");
                OnNewDevice.Invoke(device);
            }
        }

    }

    public string GetDeviceInfo(SteamVR_TrackedObject.EIndex index, ETrackedDeviceProperty property)
    {
        var system = OpenVR.System;
        if (system == null || index == SteamVR_TrackedObject.EIndex.None)
            return null;

        var error = ETrackedPropertyError.TrackedProp_Success;
        var capacity = system.GetStringTrackedDeviceProperty((uint)index, property, null, 0, ref error);
        if (capacity <= 1)
        {
            return null;
        }

        var buffer = new System.Text.StringBuilder((int)capacity);
        system.GetStringTrackedDeviceProperty((uint)index, property, buffer, capacity, ref error);

        var s = buffer.ToString();
        return s;
    }

    private void OnNewPoses(TrackedDevicePose_t[] poses)
    {
        foreach (var dev in trackedDevices)
        {
            var i = (int)dev.DeviceIndex;
            if (poses.Length >= i && poses[i].bDeviceIsConnected && poses[i].bPoseIsValid)
            {
                var pose = new SteamVR_Utils.RigidTransform(poses[i].mDeviceToAbsoluteTracking);
                OnNewPose.Invoke(dev, pose.pos, pose.rot);
            }
        }
    }
}
