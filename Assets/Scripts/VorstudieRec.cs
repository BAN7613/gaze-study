﻿using Tobii.G2OM;
using Tobii.XR;
using UnityEngine;
using System.Text;
using System.IO;
using System.Collections.Generic;
using System;
using System.Collections;
using System.Threading;
using System.Threading.Tasks;

public class VorstudieRec : MonoBehaviour
{
    //string FilePath;
    public List<string> EyeTrackingDataBuffer = new List<string>();
                                                                            //References to the Objects, that shall be recordeds
    public Camera cam;
    public GameObject Ball;

    public int interval = 200;

    private int counter = 0;
   
    private string dateTimeFileName;
    private string FilePath;
    public bool LogEye2Console;

    Vector3 rayOrigin = new Vector3();
    Vector3 rayDirection = new Vector3();

    // Use this for initialization
    void Start()
    {
        counter = 0;                                                                //Initializing values and References at startup
        dateTimeFileName = DateTime.Now.ToString("MM/dd/yyyy hh-mm tt");
        dateTimeFileName = dateTimeFileName.Replace("/", "-");
        FilePath = Application.dataPath;
        FilePath = FilePath + "/Data/Vorstudie";
        Debug.Log("The EyeTracking Data will be Saved @:" + FilePath);

    }

    async void WriteCSV(List<string> Buffer, string FileName)                   //Function that writes the built string into the csv via Async Stream Writer
    {

        string fn = Path.Combine(FilePath, FileName + ".csv");

        using (FileStream fs = new FileStream(fn, FileMode.Append, FileAccess.Write, FileShare.ReadWrite))
        using (StreamWriter sw = new StreamWriter(fs))
        {
            if (counter == 0)
            {
                await sw.WriteLineAsync("UnixTimeStamp;Time since GameStart;Round;TargetActivation;ActiveTarget;GazeRay isValid;TriggerState;BallPositionX;BallPositionY;BallPositionz;BallLockedonTarget;GazeHitPointX;GazeHitPointY;GazeHitPointZ;Gaze to Target Distance; Ball to Target Distance");
                counter += 1;
            }
            foreach (var item in Buffer)
            {
                await sw.WriteLineAsync(item);
            }
            sw.Flush();
        }
    }

    public string CaptureEyeTrackingData()                          //Collect data needed for the Logging file and Buildt a string ofit
    {
        var gazeRay = TobiiXR.EyeTrackingData.GazeRay;
        rayOrigin = TobiiXR.EyeTrackingData.GazeRay.Origin;
        rayDirection = TobiiXR.EyeTrackingData.GazeRay.Direction;

        string SystemTime = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds().ToString();
        string GameTime = Time.time.ToString();
        string rayOriginStr = V3toString(rayOrigin);
        string rayDirectionStr = V3toString(rayDirection);
        string gazeValidity = TobiiXR.EyeTrackingData.GazeRay.IsValid.ToString();

        string GazeRayhitPoint = V3toString(EyeTracking_GazeDetect.instance.GazeRayHitPoint(TobiiXR.EyeTrackingData.GazeRay.Origin, TobiiXR.EyeTrackingData.GazeRay.Direction));
        string GazeTargetDist = EyeTracking_GazeDetect.instance.GazeToTargetDist(GameManager.instance.ActiveTargetNr).ToString();
        string BallTargetDist = EyeTracking_GazeDetect.instance.BallToTargetDist(GameManager.instance.ActiveTargetNr).ToString();

        string triggerstate = ActionHandler.instance.isTriggerpressed.ToString();
        string BallPos = V3toString(Ball.transform.position);
        string BallState = Ballmanager.instance.Ballhit.ToString();

        string round = GameManager.instance.ThrowCounter.ToString();
        string ActTarget = resolveTargetID(GameManager.instance.ActiveTargetNr);
        string tarActbool = ActionHandler.instance.targetActButtonPress.ToString();
        string gazeatBullsEye = GazeOnBullsEye();

        string FrameDataBuffer = $"{SystemTime};{GameTime};{round};{tarActbool};{ActTarget};{gazeValidity};{triggerstate};" +
                                 $"{BallPos};{BallState};{GazeRayhitPoint};{GazeTargetDist};" +
                                 $"{BallTargetDist};{gazeatBullsEye}";

        if (LogEye2Console)
        {
            Debug.Log(FrameDataBuffer);
        }

        return FrameDataBuffer;
    }

    public string V3toString(Vector3 vec)                               //Translates a Vector to a String
    {
        string formattedVec = (vec.x + ";" + vec.y + ";" + vec.z);
        return formattedVec;
    }

    void tempSaveEyeTrackingData()                          //Temporarily save Data Blocks from Main Buffer
    {

        List<string> TempList = new List<string>();
        foreach (var item in EyeTrackingDataBuffer)
        {
            TempList.Add(item);
        }

        EyeTrackingDataBuffer.Clear();
        Task.Factory.StartNew(() => WriteCSV(TempList, $"{dateTimeFileName.Replace(" ", "_")} {RecordingController.Instance.ExperimentName.ToString()}_Part_{RecordingController.Instance.SubjectID.ToString()}"));
    }

    // Update is called once per frame
    void Update()
    {
        EyeTrackingDataBuffer.Add(CaptureEyeTrackingData());        //Call Tempsave Function every x Time Interval

        if (Time.frameCount % interval == 0)
        {
            tempSaveEyeTrackingData();
            
        }
    }


    public string resolveTargetID(int targetNr)                 //Translate Target ID into readable Text for Logging
    {
        switch (targetNr)
        {
            case 0:
                string left = "left (5.961, 0.37, 3)";
                return left;
            case 1:
                string middle = "middle (5.961, 0.37, 0)";
                return middle;
            case 2:
                string right = "right (5.961, 0.37, -3)";
                return right;
            case 99:
                string end = "end of Game";
                return end;
            default:
                string error = "ERROR couldnt resolve TargetID";
                return error;
        }
    }

    public void SaveTrackingData()              //Main Save Function
    {
        WriteCSV(EyeTrackingDataBuffer, $"{dateTimeFileName.Replace(" ", "_")} {RecordingController.Instance.ExperimentName.ToString()}_Part_{RecordingController.Instance.SubjectID.ToString()}");
    }

    void OnApplicationQuit()                //Before closing App, save the Data
    {
        SaveTrackingData();
        counter += 1;
    }

    public string GazeOnBullsEye()          //Check if Gaze Point Distance is within BullsEye Circle
    {
        if (EyeTracking_GazeDetect.instance.GazeToTargetDist(GameManager.instance.ActiveTargetNr) <= 0.2450001f)
        {
            return "true";
        }
        else
        {
            return "false";
        }
    }
}
