﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;
using System.IO;
using System;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine.UI;

public class QuestionnaireUIHandler : MonoBehaviour
{

    public static QuestionnaireUIHandler instance;

    public List<Question> FAM = new List<Question>();

    public GameObject QuestionnaireWindow;
    public GameObject QuestionnaireConsole;
    public GameObject QuestionnaireCanvas;
    public GameObject[] AnswerScale = new GameObject[7];
    public GameObject deleteButton;
    public GameObject nextButton;
    public GameObject nextButtonText;
    public Image QuestionnaireBackground;

    public Material selectedMat;
    public Material defaultMat;

    public Text questionText;

    public ScaleButton currentAnswer;

    public string currentQuestion;

    public Question singleQuestion = new Question("Ich glaube ich werde ein gutes Ergebnis erziehlen.", "Erfolgswahrscheinlichkeit", false);





    // Start is called before the first frame update
    void Start()
    {
        toggleQuestionnaireElements(false);

        FAM.Add(new Question("Ich finde diese Aufgabe interessant.", "Interesse", false));
        FAM.Add(new Question("Ich glaube, der Schwierigkeit dieser Aufgabe gewachsen zu sein.", "Erfolgswahrscheinlichkeit", false));
        FAM.Add(new Question("Wahrscheinlich werde ich bei dieser Aufgabe nicht gut abschneiden.", "Erfolgswahrscheinlichkeit", true));
        //FAM.Add(new Question("Bei der Aufgabe mag ich die Rolle des Wissenschaftlers, der Zusammenhänge entdeckt.", "Interesse", false));
        FAM.Add(new Question("Ich fühle mich unter Druck, bei dieser Aufgabe gut abschneiden zu müssen.", "Misserfolgsbefürchtung", false));
        FAM.Add(new Question("Diese Aufgabe ist eine richtige Herausforderung für mich.", "Herausforderung", false));
        //FAM.Add(new Question("Nach dem Lesen der Instruktion erscheint mir die Aufgabe sehr interessant.", "Interesse", false));
        FAM.Add(new Question("Ich bin sehr gespannt darauf, wie gut ich hier abschneiden werde.", "Herausforderung", false));
        FAM.Add(new Question("Ich fürchte mich ein wenig davor, dass ich mich hier blamieren könnte.", "Misserfolgsbefürchtung", false));
        FAM.Add(new Question("Ich bin fest entschlossen, mich bei dieser Aufgabe voll anzustrengen.", "Herausforderung", false));
        FAM.Add(new Question("Bei Aufgaben wie dieser brauche ich keine Belohnung, es macht mir auch so viel Spaß.", "Interesse", false));
        FAM.Add(new Question("Es ist mir etwas peinlich, hier zu versagen.", "Misserfolgsbefürchtung", false));
        FAM.Add(new Question("Ich glaube, eine gute Punktzahl kann jeder schaffen.", "Erfolgswahrscheinlichkeit", false));
        FAM.Add(new Question("Ich glaube, ich erreiche keine hohe Punktzahl.", "Erfolgswahrscheinlichkeit", true));
        FAM.Add(new Question("Wenn ich eine hohe Punktzahl erreiche, werde ich schon ein wenig stolz auf meine Leistung sein.", "Herausforderung", false));
        FAM.Add(new Question("Wenn ich an die Aufgabe denke, bin ich etwas beunruhigt.", "Misserfolgsbefürchtung", false));
        FAM.Add(new Question("So etwas würde ich auch in meiner Freizeit gerne machen. ", "Interesse", false));
        FAM.Add(new Question("Die Leistungsanforderungen hier lähmen mich.", "Misserfolgsbefürchtung", false));



        currentAnswer = null;
        questionText.gameObject.GetComponent<Text>();
        nextButton.GetComponent<MeshRenderer>().material = selectedMat;

        SetQuestioninUI(currentQuestion);

    }

    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SetBackgroundColor(bool singleQuestion)
    {
        if (singleQuestion)
        {
            QuestionnaireBackground.color = new Color(182, 0, 200);s
        }
        else
        {
            //QuestionnaireBackground.color = new Color(62, 64, 120);
            QuestionnaireBackground.color = new Color(0, 8, 245);
        }
    }

    public void toggleQuestionnaireElements(bool on)
    {
        QuestionnaireCanvas.SetActive(on);
        QuestionnaireConsole.SetActive(on);
        QuestionnaireWindow.SetActive(on);
    }


    public void OnlyColorButton(ScaleButton button)
    {
        foreach (GameObject gameObject in AnswerScale)
        {
            gameObject.GetComponent<MeshRenderer>().material = defaultMat;
        }
        button.GetComponent<MeshRenderer>().material = selectedMat;
    }

    public void SetQuestioninUI(string question)
    {
        questionText.text = question;
    }

    internal bool PrepareNextQuestion()
    {
        if(QuestionnaireManager.instance.questionsToAsk.Count > 0)
        {
            SetQuestioninUI(QuestionnaireManager.instance.questionsToAsk.First.Value.getItem);
            return true;
        }
        else
        {
            return false;
        }
    }

    public void StartQuestionaire()
    {
        DeleteButtonSelected();
        PrepareNextQuestion();
        toggleQuestionnaireElements(true);
    }

    public void ButtonSelected(ScaleButton button)
    {
        nextButton.SetActive(true);
        nextButtonText.SetActive(true);
        currentAnswer = button;
        OnlyColorButton(currentAnswer);
    }

    public void DeleteButtonSelected()
    {
        nextButton.SetActive(false);
        nextButtonText.SetActive(false);
        if (currentAnswer != null)
        {
            currentAnswer.GetComponent<MeshRenderer>().material = defaultMat;
            currentAnswer = null;
        }
        //deleteButton.GetComponent<MeshRenderer>().material = defaultMat;
        nextButton.GetComponent<MeshRenderer>().material = selectedMat;
    }

    

    public void ConfirmButtonSelected()
    {
        if (currentAnswer)
        {
            Debug.Log("Answer: " + currentAnswer.ButtonValue + " Selected & Confirmed with Next");

            QuestionnaireManager.instance.logAnswer(QuestionnaireManager.instance.questionsToAsk.First.Value.getItem, currentAnswer.ButtonValue);
            QuestionnaireManager.instance.questionsToAsk.RemoveFirst();

            DeleteButtonSelected();

            if (PrepareNextQuestion())
            {
                // Have more questions
                // Make sure everything is still visible
                toggleQuestionnaireElements(true);
            }
            else
            {
                // No more questions currently
                toggleQuestionnaireElements(false);
                GameManager.instance.ContinueStudy();
            }

            //if (GameManager.instance.first && QuestionnaireManager.instance.questionsToAsk.Count == 0) 
            //{ 
            //    GameManager.instance.StartGame(); 
            //} 

            //if (QuestionnaireManager.instance.questionsToAsk.Count != 0 && QuestionnaireManager.instance.fullQuestionnaireActive)
            //{
            //    SetQuestioninUI(QuestionnaireManager.instance.questionsToAsk.Peek().getItem);
            //    toggleQuestionnaireElements(true);
            //}
            //else
            //{
            //    toggleQuestionnaireElements(false);
            //    QuestionnaireManager.instance.fullQuestionnaireActive = false;
            //    if (GameManager.instance.ActGame)
            //    {
            //        QuestionnaireManager.instance.questionsToAsk.Push(FAM[GameManager.instance.FAMQuestionNrDuringTrial]);
            //        GameManager.instance.NewRound();
            //    }
            //}
        }
    }
}
