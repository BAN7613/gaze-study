﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestionnaireManager : MonoBehaviour
{
    public static QuestionnaireManager instance;

    public int nrFinishedQuestions;
    public bool fullQuestionnaireActive;

    public LinkedList<Question> questionsToAsk = new LinkedList<Question>();

    //public Stack<Question> questionsToAsk = new Stack<Question>();
    public List<answer> AnswersduringTrial = new List<answer>();
    public List<answer> AnswersFullQuestionnaire = new List<answer>();

    private int QuestionID = 0;

    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        nrFinishedQuestions = 0;
    }

    // Update is called once per frame
    void Update()
    {
       
    }

    public void logAnswer(string question, int answer)
    {
        RecordingController.Instance.Info.AddSetting($"{QuestionID++} - {question}", $"{answer}");
        if (fullQuestionnaireActive)
        {
            AnswersFullQuestionnaire.Add(new answer(question, answer));
        }
        else
        {
            AnswersduringTrial.Add(new answer(question, answer));
        }
        nrFinishedQuestions++;
    }


    internal void PrepareFAMQuestionaire()
    {
        questionsToAsk.Clear();
        QuestionnaireUIHandler.instance.SetBackgroundColor(false);
        foreach (Question q in QuestionnaireUIHandler.instance.FAM)
        {
            questionsToAsk.AddLast(q);
        }
    }

    internal void PrepareSingleQuestion()
    {
        questionsToAsk.Clear();
        QuestionnaireUIHandler.instance.SetBackgroundColor(true);
        questionsToAsk.AddLast(QuestionnaireUIHandler.instance.singleQuestion);
    }

    public void startFullQuestionnaire(){
        nrFinishedQuestions = 0;
        fullQuestionnaireActive = true;
        GameManager.instance.nrFullFAM++;
        foreach (Question q in QuestionnaireUIHandler.instance.FAM)
        {
           questionsToAsk.AddLast(q);
        }
    }


    [System.Serializable]
    public class answer {
        public string _question { get; set; }
        public int _answer { get; set; }


        public answer(string q, int a)
        {
            _answer = a;
            _question = q;
        }

        public override string ToString()
        {
            return _question + _answer.ToString() ;
        }
    }

}


