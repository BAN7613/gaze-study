﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Timer : MonoBehaviour
{
    public static Timer instance;
    public Text Zeit;
    public float startTime = 0;
    public int stageTime = 0;
    private int min;
    private int sec;
    public bool gameFinished;
    public bool gameStarted;
    private bool Timeisup;
    private const int countdown = 180;
    private int actTime;

    private int startcount = 0;

    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
        }
    }

    void Start()
    {
        gameFinished = false;
     gameStarted = false;
     Timeisup = false;
        actTime = 180;
}

    void Update()
    {
        
        
        if (gameStarted && startcount == 0)
        {
            startcount += 1;
            //Debug.Log("enter gameStarted");
            startTime = Time.time;
        }
        else if (gameStarted && startcount != 0 && !gameFinished && actTime > 0)
        {
            stageTime = (int)(Time.time - startTime);
        
            actTime = countdown - stageTime;
          
             //Debug.Log("enter gameStarted + running");
             min = Mathf.FloorToInt(actTime / 60);
            sec = Mathf.FloorToInt(actTime % 60);
            
        }
        else if (Timeisup == false && actTime == 0)
        {
            EndTimer();
            Timeisup = true;
        }
        else
        {
            return;
        }

        if (actTime == 15)
        {
            GetComponent<Text>().color = Color.yellow;
        }
        else if (actTime == 10)
        {
            GetComponent<Text>().color = Color.red;
        }
        
    }

    void OnGUI()
    {
        Zeit.text = min.ToString() + ":" + sec.ToString("00");
    }

    public void StartTimer()
    {
        gameStarted = true;
        gameFinished = false;
        Timeisup = false;
    }

    public void EndTimer()
    {
        gameFinished = true;
        gameStarted = false;
    }

}