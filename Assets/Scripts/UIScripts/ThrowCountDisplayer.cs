﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ThrowCountDisplayer : MonoBehaviour
{
    public Text CounterText;
    public static int wuerfe;

    void Start()        //Initialize Number of throws for Startup
    {
        wuerfe = 0;
    }

    void OnGUI()                            //Apply Number of throws to GUI Element
    {
        CounterText.text = wuerfe.ToString();
    }

}