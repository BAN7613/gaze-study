﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class pointsDisplay : MonoBehaviour
{
    public Text PointsText;
    public static int Points;

    void Start()            //Initialize Points to Zero for Display in scene
    {
        Points = 0;
    }

    void OnGUI()                //Display the Points String that was set.
    {
        PointsText.text = Points.ToString();
    }

}