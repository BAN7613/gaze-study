﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class buttonDelete : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        GetComponent<MeshRenderer>().material = QuestionnaireUIHandler.instance.selectedMat;
        QuestionnaireUIHandler.instance.DeleteButtonSelected();
    }
}
