﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class gameTimeDisplayer : MonoBehaviour
{
    public Text Zeit;
    public float startTime = 0;
    public int stageTime = 0;
    private int min;                                    //Initialize times and vlues needed to Display Game Time in Scene
    private int sec;
    public bool gameFinished = false;
    public bool gameStarted = false;

    private int startcount = 0;

    void Start()
    {

    }

    void Update()
    {
        if (gameStarted && startcount == 0)                             //Start Time counter
        {
            startcount += 1;
            startTime = Time.time;
        }
        else if (gameStarted && startcount != 0 && !gameFinished)           //refresh time display for secs and mins
        {
            stageTime = (int)(Time.time - startTime);
            min = Mathf.FloorToInt(stageTime / 60);
            sec = Mathf.FloorToInt(stageTime % 60);
        }
    }

    void OnGUI()                                                    //put time calculated on the display in the scene 
    {
        Zeit.text = min.ToString() + ":" + sec.ToString("00");
    }

}
