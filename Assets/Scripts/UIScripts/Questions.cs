﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Question
{
    public string Item;
    public string Category { get; set; }
    public bool Inverted { get; set; }


    public string getItem
    {

        get { return Item; }
    }

    public string getCategory
    {
        get { return Category; }
    }

    public bool getInverted
    {
        get { return Inverted; }
    }
    public Question(string item, string category, bool inverted)
    {
        Item = item;
        Category = category;
        Inverted = inverted;
    }

    
}