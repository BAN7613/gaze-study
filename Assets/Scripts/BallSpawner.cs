﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallSpawner : MonoBehaviour
{

    public Rigidbody ballPrefab;
    private Rigidbody firstBall;

    // Start is called before the first frame update
    void Start()
    {
      //  spawn();
    }

    public Rigidbody spawn()                                            //Method that handles the firs Appearance of the Ball
    {   
        Rigidbody ballInstance;
        ballInstance = Instantiate(ballPrefab);
        ballInstance.transform.position = transform.position;           //Set Position of Ball and return Instance of the Ball 
        return ballInstance;
    }
}





