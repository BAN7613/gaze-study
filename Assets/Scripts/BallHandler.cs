﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallHandler : MonoBehaviour
{
    public static BallHandler instance;
    private int ballToRemove;
    public GameObject BallSpawnPos;
    public Collider BallCollider;
    public ParticleSystem Explosion;

    public bool Ballhit;

    public bool ready4nextRound;


    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
        }

        Ballhit = false;


    }

    private void OnTriggerEnter(Collider Ball)
    {
        Ballhit = true;
        BallCollider = Ball;
        // GetComponent<BoxCollider>().enabled = false;
        if (Ball.attachedRigidbody != null)
            Ball.attachedRigidbody.isKinematic = true;
        Explode(Ball.transform.position);
        Debug.Log("Ball Entered Ground Collider");
    }

    public IEnumerator ResetBall(Collider Ball)
    {
        if (Ball.tag == "Ball")
        {
            Debug.Log("ResetBall meth. Aufgerufen");
            if (Ball.attachedRigidbody != null)
            {
                Ball.attachedRigidbody.velocity = Vector3.zero;
                Ball.attachedRigidbody.angularVelocity = Vector3.zero;
            }


            yield return new WaitForSeconds(0.1f);


            Ballhit = false;
            Ball.transform.position = BallSpawnPos.transform.position;
            if (Ball.attachedRigidbody != null)
                Ball.attachedRigidbody.isKinematic = false;
            GetComponent<BoxCollider>().enabled = true;


            ready4nextRound = true;

        }
    }

    public void ForceBallReset(GameObject Ball)
    {
        var BallCol = Ball.GetComponent<Rigidbody>();
        BallCol.velocity = Vector3.zero;
        BallCol.angularVelocity = Vector3.zero;
        Ball.transform.position = BallSpawnPos.transform.position;
    }

    void Explode(Vector3 Position)
    {
        var exp = Explosion;
        exp.transform.position = Position;
        exp.Play();
    }
}
