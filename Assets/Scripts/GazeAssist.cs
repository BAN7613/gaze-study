﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tobii.HEC;
using Tobii.XR;

public class GazeAssist : MonoBehaviour
{

    static public GazeAssist instance;

    public GameObject GazeMarker;
    public GameObject TobiiPoint;
    public GameObject WeightetHitPoint;

    // Fields related to the gaze focused objects.
    private GameObject _focusedTargetGameObject;
    private float _focusedTargetGameObjectTime;
    public Vector3 _focusedDirection;
    public Vector3 _focusedOrigin;
    public Vector3 _focusedGazeHitPoint;
    public Vector3 _motorThrowHitPoint;
    public float _travelTime;
    private GameObject Ball;
    private Rigidbody BallRB;
    private GazeThrowableObject GTBall;

    private List<Vector3> positionList;
    static float h;
    public Collider _hitObject;

    [Range(10.0f, 90.0f)] public float _angle;

    [Range(0f, 1f)] public float _gazeWeighting;

    private readonly float _gravity = Physics.gravity.y;

    // The time before clearing a focused object after it has lost focus.
    private const float GazeObjectMemoryTimeInSeconds = 0.2f;

    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
        }

        //Can use a less precise h to speed up calculations
        //Or a more precise to get a more accurate result
        //But lower is not always better because of rounding errors
        h = Time.fixedDeltaTime * 1f;
    }

    // Start is called before the first frame update
    void Start()
    {
        initWeight();                                                   //initialize Values needed for Gaze detection and Assist
        Ball = GameManager.instance.Ball;
        BallRB = Ball.GetComponent<Rigidbody>();
        GTBall = Ball.GetComponent<GazeThrowableObject>();
        GazeMarker.GetComponent<MeshRenderer>().enabled = false;
        TobiiPoint.GetComponent<MeshRenderer>().enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        UpdateFocusedObject();                                          //every frame the Tobii Method of updating the focusedobject in  the Buffer is called to always have the exact eyetracking data.
    }

    public void spawnGazeTargetMarker()                                 //Method that will spawn the Debuggig points for simulated motor throw Point
    {
        if (GameManager.instance.SpawnDebugPoints)
        {
            GazeMarker.GetComponent<MeshRenderer>().enabled = true;
        }
        else
        {
            GazeMarker.GetComponent<MeshRenderer>().enabled = false;
        }
            

        GazeMarker.transform.position = PredictProjectileMovement(ActionHandler.instance.Ball.transform.position, ActionHandler.instance._velocityMotorThrow, 10f);
        //GazeMarker.GetComponent<BoxCollider>().enabled = true;
    }

    public void spawnTobiiPoint()                                          //Method that spawns the Debugging point for the fixated Point
    {
        Vector3 tr = calcGazeTargetPoint();
        if (GameManager.instance.SpawnDebugPoints)                                                    //when debug Points spawn is activated
        {
            TobiiPoint.GetComponent<MeshRenderer>().enabled = true;
        }
        else
        {
            TobiiPoint.GetComponent<MeshRenderer>().enabled = false;
        }
            
        TobiiPoint.transform.position = tr;
    }

    private void UpdateFocusedObject()                              //Method, thats checks wether tobii gathered new objects and gaze points or drops the elements after certain ammount of time
    {
        // Check whether Tobii XR has any focused objects.
        foreach (var focusedCandidate in TobiiXR.FocusedObjects)
        {
            var focusedObject = focusedCandidate.GameObject;
            var focusedRayDir = focusedCandidate.Direction;
            var focusedRayOrig = focusedCandidate.Origin;


            // Find the first, if any, focused object with the GazeThrowTarget component and save it.
            if (focusedObject != null && focusedObject.GetComponent<GazeThrowTarget>())
            {
                _focusedTargetGameObject = focusedObject;
                _focusedDirection = focusedRayDir;
                _focusedOrigin = focusedRayOrig;
                _focusedTargetGameObjectTime = Time.time;
                calcGazeTargetPoint();
                break;
            }
        }

        // If enough time has passed since the object was last focused, mark it as not focused.
        if (Time.time > _focusedTargetGameObjectTime + GazeObjectMemoryTimeInSeconds)
        {
            _focusedTargetGameObjectTime = float.NaN;
            _focusedTargetGameObject = null;
            _focusedDirection = Vector3.zero;
            _focusedOrigin = Vector3.zero;
            _focusedGazeHitPoint = Vector3.zero;
        }
    }


    public Vector3 CalcVelII(Vector3 Target)
    {
        //Top down view on 3d Vector for positioning
        Vector3 projectileXZPos = new Vector3(Ball.transform.position.x, 0.0f, Ball.transform.position.z);
        Vector3 targetXZPos = new Vector3(Target.x, 0.0f, Target.z);

        // rotate the object to face the target
        Ball.transform.LookAt(targetXZPos);

        // shorthands for the formula
        float R = Vector3.Distance(projectileXZPos, targetXZPos);
        float G = Physics.gravity.y;
        float tanAlpha = Mathf.Tan(_angle * Mathf.Deg2Rad);
        float H = Target.y - Ball.transform.position.y;

        // calculate the local space components of the velocity 
        // required to land the projectile on the target point 
        float Vz = Mathf.Sqrt(G * R * R / (2.0f * (H - R * tanAlpha)));
        float Vy = tanAlpha * Vz;

        // create the velocity vector in local space and get it in global space
        Vector3 localVelocity = new Vector3(0f, Vy, Vz);
        Vector3 globalVelocity = Ball.transform.TransformDirection(localVelocity);
        return globalVelocity;

    }


    public void calcThrowAngle(Vector3 targetPos) //Method to assess the Angle of the Throw
    {
        Vector3 projectileXZPos = new Vector3(Ball.transform.position.x, 0.0f, Ball.transform.position.z);
        Vector3 targetXZPos = new Vector3(targetPos.x, 0.0f, targetPos.z);

        var dir = (targetXZPos - projectileXZPos).normalized;

        float alpha = Vector3.Angle(Ball.transform.forward, dir);
        _angle = alpha;
    }

    public Vector3 calcGazeTargetPoint() //Raycasting the pixelexact Point to spawn the debugging point.
    {
        int Layer = 1 << 8;
        RaycastHit hit;
        Physics.Raycast(_focusedOrigin, _focusedDirection, out hit, Mathf.Infinity, Layer);
        Vector3 tr = hit.point;
        // Debug.Log(tr);
        _focusedGazeHitPoint = tr;

        return tr;
    }

    public Vector3 getweightetGazeBullseyeV3(Vector3 GazePoint, Vector3 Motor) //Method that calculates the Weighted final Point, the ball should hit
    {
        Vector3 WeightetPoint;
        Debug.DrawLine(GazePoint, Motor, Color.cyan, 1000f);
        Vector3 ErrorDist = Motor - GazePoint;
        WeightetPoint = GazePoint + ErrorDist * _gazeWeighting;
        if (GameManager.instance.SpawnDebugPoints)
        {
            WeightetHitPoint.GetComponent<MeshRenderer>().enabled = true;
        }
        else
        {
            WeightetHitPoint.GetComponent<MeshRenderer>().enabled = false;
        }
        WeightetHitPoint.transform.position = WeightetPoint;
        return WeightetPoint;
    }

    public Vector3 PredictProjectileMovement(Vector3 InitialPosition, Vector3 InitialVelocity, float TimeToExplode) //Method that simulates the actual motor throw of the Participant
    {
        Vector3 Position = InitialPosition;
        Vector3 Velocity = InitialVelocity;
        Vector3 GravitationalAcceleration = Physics.gravity;
        float t = 0.0f;
        float DeltaTime = 0.02f;


        while (t < TimeToExplode)
        {
            Vector3 PreviousPosition = Position;
            Vector3 PreviousVelocity = Velocity;

            //Formula to describe every point on trajectory after certain time
            Position += GravitationalAcceleration * DeltaTime * DeltaTime * 0.5f + Velocity * DeltaTime;
            Velocity += GravitationalAcceleration * DeltaTime;

            // Collision detection.
            // int Layer = 1 << 8;
            RaycastHit HitInfo;
            if (Physics.Linecast(PreviousPosition, Position, out HitInfo))
            {
                // Set the hit point as the new position.
                Position = HitInfo.point;
                //set collision of ray with obj, as final Motor Throw hit point.
                _motorThrowHitPoint = Position;
                break;
            }

            t += DeltaTime;
        }

        return Position;
    }

    private float initWeight()  //Method that evaluates what the initial weighting should be depending of chosen Condition.
    {
        switch (GameManager.instance.condition)
        {
            case GameManager.Condition.Motor:
                GazeAssist.instance._gazeWeighting = 1f;
                return GazeAssist.instance._gazeWeighting;

            case GameManager.Condition.Gaze:
                GazeAssist.instance._gazeWeighting = 0f;
                return GazeAssist.instance._gazeWeighting;

            case GameManager.Condition.Motor2Gaze:

                GazeAssist.instance._gazeWeighting = 1f;
                return GazeAssist.instance._gazeWeighting;

            case GameManager.Condition.Gaze2Motor:

                GazeAssist.instance._gazeWeighting = 0f;
                return GazeAssist.instance._gazeWeighting;

            case GameManager.Condition.BullsEye2Motor:

                GazeAssist.instance._gazeWeighting = 0f;
                return GazeAssist.instance._gazeWeighting;

            case GameManager.Condition.Motor2BullsEye:

                GazeAssist.instance._gazeWeighting = 1f;
                return GazeAssist.instance._gazeWeighting;
            default:
                GazeAssist.instance._gazeWeighting = 0f;
                return GazeAssist.instance._gazeWeighting;
        }
    }
}
