﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundBallHandler : MonoBehaviour
{
    

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider Ball)  //Wehen Ball hits the ground Plate, behave, like successful throw was completed.
    {
        if (Ball.tag == "Ball")
        {
            Ballmanager.instance.Ballhit = true;
            Ballmanager.instance.BallCollider = Ball;
            GameManager.instance.BallhitTarget();
            GetComponent<BoxCollider>().enabled = false;
            if(Ball.attachedRigidbody != null)
                Ball.attachedRigidbody.isKinematic = true;
        }
    }
}
