﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;


public class PlayerController : MonoBehaviour
{

    private Throwable _grabbedThrowable; //The Ball that's grabbed
    private Vector3 _currentGrabbedLocation; // Tracked location of the Throwable Obj.



    // Start is called before the first frame update
    void Start()
    {
        _grabbedThrowable = null;
        _currentGrabbedLocation = new Vector3();
    }

    // Update is called once per frame
    void Update()
    {
       if (_grabbedThrowable != null)
        {
            _currentGrabbedLocation = _grabbedThrowable.transform.position; //If grabbed Obj not null, save Position
        }
    }

    public void HoldGameObj (GameObject throwableObj) //Method that sets the Throwable Obj. and Parent relationship etc.
    {
        Throwable throwable = throwableObj.GetComponent<Throwable>();
        if (throwable != null)
        {
            _grabbedThrowable = throwable;
            throwableObj.transform.parent = this.gameObject.transform;
            _grabbedThrowable.GetComponent<Rigidbody>().isKinematic = true;
            _currentGrabbedLocation = _grabbedThrowable.transform.position;
        }
    }

    public void ReleaseGameObj()        //Method for releasing the Game Obj.
    {
        if (_grabbedThrowable != null)
        {
            _grabbedThrowable.transform.parent = null;
            Rigidbody rigidbody = _grabbedThrowable.GetComponent<Rigidbody>();
            rigidbody.isKinematic = false;

            Vector3 throwVector = _grabbedThrowable.transform.position - _currentGrabbedLocation;
            rigidbody.AddForce(throwVector * 10, ForceMode.Impulse);

            _grabbedThrowable = null;
        }
    }
}
