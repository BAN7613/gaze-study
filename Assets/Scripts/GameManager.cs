﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class GameManager : MonoBehaviour
{
    [Record("SubjectID")]
    public int SubjectID;                             //Field to enter SUbj ID
    public String ExpName;
    public enum Group { A, B};

    [Record("Group")]
    public Group group;
    public enum Hand { LeftHand, RightHand };           //select Hand that user is using
    public Hand hand;
    public int FAMQuestionNrDuringTrial;

    public enum GameState
    {
        BeforeStart,
        AfterEyeCalibration,
        FirstQuestionaire,
        StudyPart_1_Question,
        StudyPart_1_Throw,
        SecondQuestionaire,
        StudyPart_2_Question,
        StudyPart_2_Throw,
        End
    }

    [Record("GameState")]
    public GameState _currentState;


    public bool SkipEyeCalibration;
    public bool SpawnDebugPoints;

    public int ThrowsInExperimentCondition = 30;         //Number of throws to be thrown in Exp Condition depending on Group A or B
    public int ThrowsInBaseCondition = 15;               //Number of throws that shall be thrown in the Motor Baseline Condition played after the Experimentalt Condition

    public int nrFullFAM = 0;

    public enum Condition { Motor, Gaze, Motor2Gaze, Gaze2Motor, BullsEye2Motor, Motor2BullsEye }; //Options to select Condition that shall be Played in Unity

    [Record("Condition")]
    public Condition condition;
    
                     //Debug Points shall be displayed or not?
   
    [Record("ThrowCounter")]   
    public int ThrowCounter;
    public bool first;
    [Record("ActiveTargetNr")]
    public int ActiveTargetNr;
    public bool ActGame;
    public bool finishedFirstCond;

    [Record("Points")]
    public int Points;


    [Record("SysTimestampGamestart")]
    public string SysTimestampGamestart;
    [Record("GameTimestampstart")]
    public string GameTimestampstart;
    [Record("GameTimestampEnd")]
    public string GameTimestampEnd;
    private string SysTimestampGameEnd;

    public Material BWMaterial;
    public Material ColMaterial;

    public GameObject Ball;
    public GameObject Punkte;
    public GameObject Wuerfe;
    public GameObject TargetPodest;
    public GameObject StudyEndText;
    public GameObject QuestionnaireWindow;

    public int oldTarg;
    private bool prepped = false;

    public static GameManager instance;
    public gameTimeDisplayer clock;


    private System.Random rndm;
    private ScaleButton[] allScaleButtons;

    // used to start the timer only when player throws first ball
    public static bool startTimer;

    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
        }

        switch (group)
        {
            case Group.A:
                condition = Condition.Gaze2Motor;
                break;
            case Group.B:
                condition = Condition.Motor;
                break;
        }

        _currentState = GameState.BeforeStart;

        //_currentState = GameState.StudyPart_1_Throw;
        ThrowCounter = 0;
        ActGame = true;
        first = true;
        Ball.SetActive(false);
        StudyEndText.SetActive(false);
    

        rndm = new System.Random();

        RecordingController.Instance.SubjectID = SubjectID;
        RecordingController.Instance.ExperimentName = ExpName;

    }

    // Start is called before the first frame update
    void Start()
    {
        allScaleButtons = FindObjectsOfType<ScaleButton>();
    }

    public void ContinueStudy()
    {
        switch (_currentState)
        {
            case GameState.BeforeStart:
                Debug.Log("Started Calibration");
                if (!SkipEyeCalibration)
                {
                    // 1 This is a blocking call - expect temporary freeze
                    ViveSR.anipal.Eye.SRanipal_Eye_v2.LaunchEyeCalibration();
                }
                Debug.Log("Finished Calibration");
                _currentState = GameState.AfterEyeCalibration;
                RecordingController.Instance.StartRecording();
                ContinueStudy();
                break;
            case GameState.AfterEyeCalibration:
                // 2 Start Full Questionaire
                QuestionnaireManager.instance.PrepareFAMQuestionaire();
                QuestionnaireUIHandler.instance.StartQuestionaire();
                _currentState = GameState.FirstQuestionaire;
                break;
            case GameState.FirstQuestionaire:
            case GameState.StudyPart_1_Throw:
                // 6 Are we finished with this part?
                if(ThrowCounter == ThrowsInExperimentCondition)
                {
                    QuestionnaireManager.instance.PrepareFAMQuestionaire();
                    QuestionnaireUIHandler.instance.StartQuestionaire();
                    _currentState = GameState.SecondQuestionaire;
                    break;
                }

                // 3 Ask First Question and Throw 
                Ball.SetActive(false);
                QuestionnaireManager.instance.PrepareSingleQuestion();
                QuestionnaireUIHandler.instance.StartQuestionaire();
                _currentState = GameState.StudyPart_1_Question;
                break;
            case GameState.StudyPart_1_Question:
                // 4 Start Clock
                if (!clock.gameStarted)
                {
                    SysTimestampGamestart = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds().ToString();
                    GameTimestampstart = Time.time.ToString();
                    clock.gameStarted = true;
                }

                // 5 Show Ball and start throw
                Ball.SetActive(true);
                NewRound();
                _currentState = GameState.StudyPart_1_Throw;
                break;
            case GameState.SecondQuestionaire:
            case GameState.StudyPart_2_Throw:
                if (ThrowCounter == ThrowsInExperimentCondition + ThrowsInBaseCondition)
                {
                    //QuestionnaireManager.instance.PrepareFAMQuestionaire();
                    //QuestionnaireUIHandler.instance.StartQuestionaire();
                    EndGame();
                    _currentState = GameState.End;
                    break;
                }
                // 7 Ask First Question and Throw 
                Ball.SetActive(false);
                QuestionnaireManager.instance.PrepareSingleQuestion();
                QuestionnaireUIHandler.instance.StartQuestionaire();
                _currentState = GameState.StudyPart_2_Question;

                break;
            case GameState.StudyPart_2_Question:
                // 8 Show Ball and start throw
                Ball.SetActive(true);
                NewRound();
                _currentState = GameState.StudyPart_2_Throw;
                break;
        }
    }

    private KeyCode[] keyCodes = {
         KeyCode.Alpha1,
         KeyCode.Alpha2,
         KeyCode.Alpha3,
         KeyCode.Alpha4,
         KeyCode.Alpha5,
         KeyCode.Alpha6,
         KeyCode.Alpha7
     };

    // Update is called once per frame
    void Update()
    {
        if(_currentState == GameState.BeforeStart && Input.GetKeyDown(KeyCode.Space)){
            ContinueStudy();
        }
        

        //if (Input.GetKeyDown("p"))
        //{
        //    ViveSR.anipal.Eye.SRanipal_Eye_v2.LaunchEyeCalibration();
        //}

        //if (Input.GetKeyDown("s"))      //Press Key S to start game / spawn Ball
        //{
        //    // QuestionnaireManager.instance.questionsToAsk.Push(QuestionnaireUIHandler.instance.FAM[FAMQuestionNrDuringTrial]);
        //    QuestionnaireManager.instance.startFullQuestionnaire();
        //    StartQuestionnaire();
        //}

        //if (Input.GetKeyDown("e"))          //Press E to force Game to quit
        //{
        //    EndGame();
        //}

        if (Input.GetKeyDown("r"))              //Press R to reset the Ball
        {
            BallHandler.instance.ForceBallReset(Ball);
        }

        if(_currentState == GameState.StudyPart_1_Question || 
            _currentState == GameState.StudyPart_2_Question ||
            _currentState == GameState.FirstQuestionaire ||
            _currentState == GameState.SecondQuestionaire)
        {
            for (int i = 0; i < keyCodes.Length; i++)
            {
                if (Input.GetKeyDown(keyCodes[i]))
                {
                    for (int j = 0; j < allScaleButtons.Length; j++)
                    {
                        if (allScaleButtons[j].ButtonValue == i + 1)
                        {
                            QuestionnaireUIHandler.instance.ButtonSelected(allScaleButtons[j]);
                        }
                    }
                }
            }

            //if (Input.GetKeyDown(KeyCode.Alpha1))
            //{
            //    var btn = FindObjectsOfType<ScaleButton>();
            //    if(btn.Length > 0)
            //        QuestionnaireUIHandler.instance.ButtonSelected(btn[UnityEngine.Random.Range(0, btn.Length)]);

            //}

            if (Input.GetKeyDown(KeyCode.Alpha0))
            {
                QuestionnaireUIHandler.instance.ConfirmButtonSelected();

            }
        }

        



        // if (ThrowCounter > (ThrowsInExperimentCondition-1) && ActGame == true) //Automatically end Game after desired number of throws have been reached
        //  {
        //      EndGame();           
        //  }


    }

    public void StartQuestionnaire()
    {
        //Debug.Log("Started Questionnaire");

        QuestionnaireManager.instance.PrepareFAMQuestionaire();
        QuestionnaireUIHandler.instance.StartQuestionaire();
        //QuestionnaireUIHandler.instance.SetQuestioninUI(QuestionnaireManager.instance.questionsToAsk.First.Value.Item);
        //QuestionnaireUIHandler.instance.toggleQuestionnaireElements(true);
    }

    public void StartGame()  //Set values and initiates everything needed to start the game
    {
        ActGame = true;
        switch (group)
        {
            case Group.A:
                condition = Condition.Gaze2Motor;
                break;
            case Group.B:
                condition = Condition.Motor;
                break;

        }       
        StudyEndText.SetActive(false);
        Debug.Log("Started the Game @" + DateTime.Now);
        Ball.SetActive(true);
        SysTimestampGamestart = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds().ToString();
        GameTimestampstart = Time.time.ToString();
        clock.gameStarted = true;
        
        NewRound();

    }

    private void EndGame() //Set values and everything to force end the Game also save the Data
    {
        ActGame = false;
        
        // StudyEndText.SetActive(true);
        Debug.Log("Ended the Game @" + DateTime.Now);
        SysTimestampGameEnd = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds().ToString();
        GameTimestampEnd = Time.time.ToString();
        clock.gameFinished = true;
        Ball.SetActive(false);
        MaterialManager.instance.DeactivateTarg();
        EyeTrackingCSV_Recorder.instance.EyeTrackingDataBuffer.Add(EyeTrackingCSV_Recorder.instance.CaptureStudyInfo(SysTimestampGameEnd, GameTimestampEnd, "GameEnd", 99));
        //QuestionnaireManager.instance.startFullQuestionnaire();
        StudyEndText.SetActive(true);
        //StartQuestionnaire();
    }

    public void NewRound()     //Method that handles everything needed to initiate New Round for the next throw
    {
        oldTarg = ActiveTargetNr;
        ActionHandler.instance.lockedGazePoint = Vector3.zero;
        ActionHandler.instance.targetActButtonPress = false;
        
        if (first)
        {                                                   //Also track Information of when either game started or new round starts.
            Ballmanager.instance.ready4nextRound = true;
            EyeTrackingCSV_Recorder.instance.EyeTrackingDataBuffer.Add(EyeTrackingCSV_Recorder.instance.CaptureStudyInfo(SysTimestampGamestart, GameTimestampstart, "GameStart", ActiveTargetNr));
            first = false;

            // Increment here as well
            IncrementThrowCounter(1);

        }
        else
        {
            if (ActGame)
            {
                EyeTrackingCSV_Recorder.instance.EyeTrackingDataBuffer.Add(EyeTrackingCSV_Recorder.instance.CaptureStudyInfo(SysTimestampGamestart, GameTimestampstart, "NewRound", ActiveTargetNr));

                StartCoroutine(Ballmanager.instance.ResetBall(Ballmanager.instance.BallCollider));
                
            }
        }
    }

    public void BallhitTarget()     //When ball hit Target is called it Updates the GUI Fields Points, Throws etc.
    {
        UpdateGameValues();

        // Check what happens next
        ContinueStudy();

        //if (nrFullFAM <= 2)
        //{
        //    if (ThrowCounter <= ThrowsInExperimentCondition)
        //    {
        //        StartQuestionnaire();
        //    }
        //    else if (!prepped)
        //    {
        //        prepBaseCond();
        //    }
        //    else
        //    {
        //        StartQuestionnaire();
        //    }
        //}
        //else
        //{
        //    EndGame();
        //}
      
    }

    public void IncrementThrowCounter(int incrby)             //Setter Methods for all GUI Values
    {
        ThrowCounter = ThrowCounter + incrby;
        ThrowCountDisplayer.wuerfe = ThrowCounter;
    }

    private void SetPointCounter(int pt)
    {
        pointsDisplay.Points = Points;
    }

    private int GetThrowCounter()
    {
        return ThrowCounter;
    }

    private void UpdateGameValues()     //Update the Scripts for displaying the Values set.
    {
       
        int PointsThisRound = ScoreCalc.instance.DistanceToPoints(ScoreCalc.instance.CalcDistancetoBullsEye(Ball.transform.position, ActiveTargetNr));
        MainStudy_Recorder.instance.ScoreperRound = PointsThisRound;
        if(ThrowCounter > ThrowsInExperimentCondition)
        {
            Points = ScoreCalc.instance.sumScore(PointsThisRound);
            SetPointCounter(Points);
        }
    }

    private void prepBaseCond()
    {
        prepped = true;
            QuestionnaireManager.instance.startFullQuestionnaire();
            StartQuestionnaire();

            condition = Condition.Motor; 
    }
}
