using Tobii.G2OM;
using Tobii.XR;
using UnityEngine;
using System.Text;
using System.IO;
using System.Collections.Generic;
using System;
using System.Collections;
using System.Threading;
using System.Threading.Tasks;



//Monobehaviour which implements the "IGazeFocusable" interface, meaning it will be called on when the object receives focus
public class EyeTrackingCSV_Recorder : MonoBehaviour
{
    public static EyeTrackingCSV_Recorder instance;
    //List as Buffer for Eyetracking Information
    public List<string> EyeTrackingDataBuffer = new List<string>();
    //Create Vector Variables for Origin, Direction Screen Position
    Vector3 rayOrigin = new Vector3();
    Vector3 rayDirection = new Vector3();
    Vector3 screenPosition = new Vector3();


    public Camera cam;
    public GameObject Ball;
    public bool LogEye2Console;     //Bool wether Eyetracking info should be logged to Console


    public int interval = 200;

    private int counter = 0;

    //references to Dateand Time File Name as well as File Path for the recorded Data
    private string dateTimeFileName;
    private string FilePath;



    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
        }
    }

    private void Start()
    {

        //Create File Name and Path where the logged Data should be Saved
        dateTimeFileName = "EyeTracking-" + DateTime.Now.ToString("MM/dd/yyyy hh-mm tt");
        dateTimeFileName = dateTimeFileName.Replace("/", "-");
        FilePath = Application.dataPath;
        FilePath = FilePath + "/Data";
        //Show user in console, where Data will be saved
        Debug.Log("The EyeTracking Data will be Saved @:" + FilePath);
    }


    //Write the collected Data ansynchronously to the File.
    async void WriteCSV(List<string> Buffer, string FileName)
    {

        string fn = Path.Combine(FilePath, FileName + ".csv");

        //Open Stream Writer to fill csv. with Data.
        using (FileStream fs = new FileStream(fn, FileMode.Append, FileAccess.Write, FileShare.ReadWrite))
        using (StreamWriter sw = new StreamWriter(fs))
        {
            if (counter == 0)
            {
                //Header at top of CSV
                await sw.WriteLineAsync("UnixTimeStamp;Time since GameStart;Origin V3 X;Y;Z;Direction V3 X;Y;Z;GazeRay isValid;isLeftEyeBlinking;isRightEyeBlinking;ScreenPositionfromWorldspace pixelWidth;pixelHeight;WorldUnitsfromCamera;TriggerState;BallPositionX;Y;Z;BallLockedonTarget;GazeHitPoint;Gaze to Target Distance; Ball to Target Distance");
                await sw.WriteLineAsync("SubjectID: " + RecordingController.Instance.SubjectID.ToString() + ";" + "ExperimentName: " + RecordingController.Instance.ExperimentName.ToString());
                counter += 1;
            }
            foreach (var item in Buffer)
            {
                //Write Data to file
                await sw.WriteLineAsync(item);
            }
            //Empty the Stream Writer after everything got written
            sw.Flush();
        }
    }


    public string CaptureEyeTrackingData()  //Method that collects all the information that sould be Logged from all the Scripts
    {
        var gazeRay = TobiiXR.EyeTrackingData.GazeRay;
        rayOrigin = TobiiXR.EyeTrackingData.GazeRay.Origin;
        rayDirection = TobiiXR.EyeTrackingData.GazeRay.Direction;
        float Convergence = TobiiXR.EyeTrackingData.ConvergenceDistance;
        var isLeftEyeBlinking = TobiiXR.EyeTrackingData.IsLeftEyeBlinking;
        var isRightEyeBlinking = TobiiXR.EyeTrackingData.IsRightEyeBlinking;

        string SystemTime = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds().ToString();
        string GameTime = Time.time.ToString();
        string rayOriginStr = V3toString(rayOrigin);
        string rayDirectionStr = V3toString(rayDirection);
        string gazeValidity = TobiiXR.EyeTrackingData.GazeRay.IsValid.ToString();
        string leftEyeBlinking = isLeftEyeBlinking.ToString();
        string rightEyeBlinking = isRightEyeBlinking.ToString();
        string GazeRayhitPoint = V3toString(EyeTracking_GazeDetect.instance.GazeRayHitPoint(TobiiXR.EyeTrackingData.GazeRay.Origin, TobiiXR.EyeTrackingData.GazeRay.Direction));
        string GazeTargetDist = EyeTracking_GazeDetect.instance.GazeToTargetDist(GameManager.instance.ActiveTargetNr).ToString();
        string BallTargetDist = EyeTracking_GazeDetect.instance.BallToTargetDist(GameManager.instance.ActiveTargetNr).ToString();

        string triggerstate = ActionHandler.instance.isTriggerpressed.ToString();
        string BallPos = V3toString(Ball.transform.position);
        string BallState = Ballmanager.instance.Ballhit.ToString();

        //Build String with all Variables that have been collected
        string FrameDataBuffer = $"{SystemTime};{GameTime};{rayOriginStr};{rayDirectionStr};{gazeValidity};{leftEyeBlinking};" +
                                 $"{rightEyeBlinking};{worldtoScreenCoord(rayOrigin, rayDirection, Convergence, cam)};{triggerstate};" +
                                 $"{BallPos};{BallState};{GazeRayhitPoint};{GazeTargetDist};{BallTargetDist}";

        if (LogEye2Console)
        {
            Debug.Log(FrameDataBuffer);
        }

        return FrameDataBuffer;
    }


    public string worldtoScreenCoord(Vector3 Origin, Vector3 Direction, float convergence, Camera cam) //Method to translate Gaze to screen coordinates
    {

        if (TobiiXR.EyeTrackingData.ConvergenceDistanceIsValid && TobiiXR.EyeTrackingData.GazeRay.IsValid)
        {
            Vector3 WorldPos = rayOrigin + rayDirection.normalized * convergence;
            screenPosition = cam.WorldToScreenPoint(WorldPos);
            string screenPositionStr = V3toString(screenPosition);
            return screenPositionStr;
        }
        else
        {
            string notValid = "NULL;NULL;NULL";
            return notValid;
        }
    }

    public string V3toString(Vector3 vec) //Method to transform Vector Info to String
    {
        string formattedVec = (vec.x + ";" + vec.y + ";" + vec.z);
        return formattedVec;
    }

    void tempSaveEyeTrackingData()      //Temporarily save Data to the TempList so nothing gets lost when system Lags or Stops
    {

        List<string> TempList = new List<string>();
        foreach (var item in EyeTrackingDataBuffer)
        {
            TempList.Add(item);
        }

        EyeTrackingDataBuffer.Clear();
        Task.Factory.StartNew(() => WriteCSV(TempList, $"{dateTimeFileName.Replace(" ", "_")} EyeTrackingData_All"));   //Call new asynchronous Task of collecting Data
    }

    private void Update()
    {

        EyeTrackingDataBuffer.Add(CaptureEyeTrackingData());       //add Data to the Buffer in every calculated Frame

        if (Time.frameCount % interval == 0)
        {
            tempSaveEyeTrackingData();                       //save the Data temporarily every interval that was Set 

        }

    }

    public string CaptureStudyInfo(string Systime, string gameTime, string StartEndNewRound, int taregtID)    //save Information of Study 
    {
        string target = resolveTargetID(taregtID);
        string StudyInfo = $"{Systime};{gameTime};{StartEndNewRound};{target}";
        return StudyInfo;
    }

    public string resolveTargetID(int targetNr)   //resolve target id to readable location
    {
        switch (targetNr)
        {
            case 0:
                string left = "left";
                return left;
            case 1:
                string middle = "middle";
                return middle;
            case 2:
                string right = "right";
                return right;
            case 99:
                string end = "end of Game";
                return end;
            default:
                string error = "ERROR couldnt resolve TargetID";
                return error;
        }
    }

    public void SaveTrackingData()    //save the tracking Data from thr Buffer.
    {
        WriteCSV(EyeTrackingDataBuffer, $"{dateTimeFileName.Replace(" ", "_")} EyeTrackingData_All");
    }

    void OnApplicationQuit()      //before Quitting Application, save the Data
    {
        //tempSaveEyeTrackingData
        SaveTrackingData();
        counter += 1;
    }
}