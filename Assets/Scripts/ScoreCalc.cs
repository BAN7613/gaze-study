﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreCalc : MonoBehaviour
{
    public static ScoreCalc instance;

    private Vector3 Ballposition;
    public GameObject BullsEyeLeft;
    public GameObject BullsEyeMiddle;
    public GameObject BullsEyeRight;

    public GameObject Measure;

    public int TotalScore;

    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
        }

    }

    // Start is called before the first frame update
    void Start()
    {
        TotalScore = 0;

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public float CalcDistancetoBullsEye(Vector3 BallPos, int TargetID)  // Method that calculates the distance of the Ball Position on Taret tothe BullsEye
    {
        switch (TargetID)
        {
            case 0:
                float distBEBPLeft = Vector3.Distance(BallPos, BullsEyeLeft.transform.position); // LEFT TARGET INDEX 0

                return distBEBPLeft;
            case 1:
                float distBEBPMiddle = Vector3.Distance(BallPos, BullsEyeMiddle.transform.position); // MIDDLE TARGET INDEX 1

                return distBEBPMiddle;
            case 2:
                float distBEBPRight = Vector3.Distance(BallPos, BullsEyeRight.transform.position); // RIGHT TARGET INDEX 2

                return distBEBPRight;
                 
            default:
                float error = 0.0f;
                return error;
        }
    }

    public int DistanceToPoints(float dist) //Method that calculates the Points reached this throw via Distance
    {
        if (0f < dist && dist < 0.2450001f)
        {
            int points = 50;
            return points;
        }
        else if (0.2450001f < dist && dist < 0.4999001f)
        {
            int points = 25;
            return points;
        }
        else if (0.4999001f < dist && dist < 0.8122001f)
        {
            int points = 10;
            return points;
        }
        else
        {
            int points = 0;
            return points;
        }
    }

    public int sumScore(int points) //Method to summ up the calculated scores
    {
        TotalScore += points;
        return TotalScore;
    }

}
